var webpack = require('webpack');
var devFlagPlugin = new webpack.DefinePlugin({
    __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
});


module.exports = {
    cache: true,
    module: {
        loaders: [
            {test: /\.js$/, loaders: ['jsx', 'babel'], exclude: /node_modules|bower|babel|marked/}
        ]
    },
    entry: {
        main: './app/react-src/main.js',
        admin: './app/react-src/admin.js'
    },

    output: {
        filename: '[name].min.js', //for minimization
        //filename: '[name].js',
        path: __dirname + '/app/static/src',
        //publicPath: 'http://localhost:8080/app/static/src'
    },

    plugins: [
        new webpack.NoErrorsPlugin(),

        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }}), //for minimization


        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),

        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],

    resolve: {
        extensions: ['', '.js', '.jsx']
    }

};