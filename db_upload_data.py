from app import db, models
import json
from random import randrange
from math import ceil
from datetime import datetime, timedelta


STATES = [
    ("AL", "Alabama"), ("AK", "Alaska"), ("AZ", "Arizona"), ("AR", "Arkansas"), ("CA", "California"),
    ("CO", "Colorado"), ("CT", "Connecticut"), ("DE", "Delaware"), ("DC", "District of Columbia"), ("FL", "Florida"),
    ("GA", "Georgia"), ("HI", "Hawaii"), ("ID", "Idaho"), ("IL", "Illinois"), ("IN", "Indiana"), ("IA", "Iowa"),
    ("KS", "Kansas"), ("KY", "Kentucky"), ("LA", "Louisiana"), ("ME", "Maine"), ("MD", "Maryland"),
    ("MA", "Massachusetts"), ("MI", "Michigan"), ("MN", "Minnesota"), ("MS", "Mississippi"), ("MO", "Missouri"),
    ("MT", "Montana"), ("NE", "Nebraska"), ("NV", "Nevada"), ("NH", "New Hampshire"), ("NJ", "New Jersey"),
    ("NM", "New Mexico"), ("NY", "New York"), ("NC", "North Carolina"), ("ND", "North Dakota"), ("OH", "Ohio"),
    ("OK", "Oklahoma"), ("OR", "Oregon"), ("PA", "Pennsylvania"), ("RI", "Rhode Island"), ("SC", "South Carolina"),
    ("SD", "South Dakota"), ("TN", "Tennessee"), ("TX", "Texas"), ("UT", "Utah"), ("VT", "Vermont"), ("VA", "Virginia"),
    ("WA", "Washington"), ("WV", "West Virginia"), ("WI", "Wisconsin"), ("WY", "Wyoming")
]


def randomTime(start_time_str):
    format = '%m/%d/%Y'

    start = datetime.strptime(start_time_str, format)
    end = datetime.now() #strptime("10/31/2015", format)
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


db.drop_all()
db.create_all()

with open('output.json') as json_data:
    types_data = json.load(json_data)
    json_data.close()

if len(models.Type.query.all()) == 0:
    for category in types_data['category']:
        print category
        db.session.add(models.Category(name=category['name']))
    for subcategory in types_data['subcategory']:
        db.session.add(models.Subcategory(name=subcategory['name'], category_id=subcategory['category']))
    for types in types_data['type']:
        db.session.add(models.Type(name=types['name'], category_id=types['category'], subcategory_id=types['subcategory']))
    for news in types_data['news']:
            db.session.add(models.News(title=news['title'], img_path=news['img_path'],text=news['text'], added=randomTime("07/08/2015")))
    db.session.commit()
    print models.Type.query.count()
    print models.News.query.count()


with open('data.json') as json_data:
    data = json.load(json_data)
    json_data.close()

if len(models.State.query.all()) == 0:
    for state in STATES:
        db.session.add(models.State(code=state[0], name=state[1]))
    db.session.commit()
    print models.State.query.count()



if len(models.Beer.query.all()) == 0:
    for beer in data:
        q = models.Type.query.filter(models.Type.name.like(beer['type'])).first()
        path = ''
        if beer['img']:
            try:
                path = beer['img']
                path = path.replace('%', '-')
                # urllib.urlretrieve(beer['img'], path)
            except IOError:
                print beer['name']
                pass
        price = 0.00
        price = beer['price']
        sold = randrange(0, 420)
        if models.Beer.query.filter(models.Beer.name.like(beer['name'])).count() == 0:
            entry = models.Beer(name=beer['name'], price=price,
                                description=beer['description'],
                                img_path=path, available=True,
                                added=randomTime("10/31/2013"), sold=sold)
        else:
            entry = models.Beer.query.filter(models.Beer.name.like(beer['name'])).first()
        entry.types.append(q)
        db.session.add(entry)
    db.session.commit()
    print(models.Beer.query.count())

## fake rating generation, takes a lot time for completion

# for rate in models.BeerUserRate.query.all():
#     db.session.delete(rate)
# db.session.commit()
# print models.BeerUserRate.query.count()
#
# for beer in models.Beer.query.all():
#     if beer.sold > 0:
#         for i in range(randrange(1, 150)):
#             rate = models.BeerUserRate(beer_id=beer.id, rate=randrange(1,5))
#             db.session.add(rate)
#         print beer.id
#     db.session.commit()