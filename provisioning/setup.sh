#!/bin/bash

echo 'Installing nodeJS and bower'
sudo apt-get install nodejs --assume-yes
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo apt-get install npm --assume-yes
sudo apt-get install git --assume-yes
sudo apt-get install nginx --assume-yes
sudo apt-get install python-dev --assume-yes
sudo apt-get install libjpeg-dev --assume-yes
sudo apt-get install libjpeg8-dev --assume-yes
sudo apt-get install libpng3 --assume-yes
sudo apt-get install libfreetype6-dev --assume-yes
sudo sed -i "s/sendfile on/sendfile off/g" /etc/nginx/nginx.conf
sudo npm install -g bower
sudo npm install -g webpack
echo 'Installing python packages'
sudo apt-get install python-pip python-dev build-essential --assume-yes
sudo pip install --upgrade pip
cd /vagrant
sudo cp provisioning/flask-beerstore /etc/nginx/sites-available/default
sudo pip install -r ./requirements.txt
echo 'Installing npm packages'
sudo npm install --no-bin-links
echo 'Installing bower packages'
sudo bower install --allow-root

echo '' >> ~/.bashrc
echo 'cd /vagrant' >> ~/.bashrc
echo "alias sgunicorn='sudo gunicorn -c unicorn_config.py wsgi:app'" >> ~/.bashrc
