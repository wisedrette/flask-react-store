import os

basedir = os.path.abspath(os.path.dirname(__file__))

# Flask-Security config
SECURITY_URL_PREFIX = "/admin"
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECURITY_PASSWORD_SALT = "ATGUOHAELKiubahiughaerGOJAEGj"

MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_SSL = False
MAIL_USE_TLS=True
MAIL_USERNAME = 'beercraft.flre@gmail.com'
MAIL_PASSWORD = 'beercraft_FLRE'
# Flask-Security URLs, overridden because they don't put a / at the end
SECURITY_LOGIN_URL = "/login/"
SECURITY_LOGOUT_URL = "/logout/"

SECURITY_POST_LOGIN_VIEW = "/admin/"
SECURITY_POST_LOGOUT_VIEW = "/admin/"
SECURITY_POST_REGISTER_VIEW = "/admin/"

WEBPACK_MANIFEST_PATH = os.path.join(basedir, 'app/build/manifest.json')
UPLOAD_FOLDER = os.path.join(basedir, 'app/static/img/avatars')
file_path = os.path.join(basedir, 'app/static')
BOOTSTRAP_SERVE_LOCAL = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True
SECRET_KEY = 'IL0veJerza'
ENTRIES_PER_PAGE = 40
