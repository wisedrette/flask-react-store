bind = "0.0.0.0:5000"
# due to socket.io limitations worker's count should be equal to 1
workers = 1
threads = 8
reload = True
sendfile = True
timeout = 120
worker_class = 'eventlet'

loglevel = 'debug'
# daemon = True