from app import db, models
import json, sys

data = []

models_list = ['Category', 'Subcategory', 'Type', 'News']

for model in models_list:
    model_class = getattr(sys.modules[models.__name__], model)
    model_data = []
    for item in model_class.query.all():
        model_data.append(item.serialize)
    data.append({model.lower(): model_data})

print getattr(sys.modules[models.__name__], 'Beer').query.all()

with open('output.json', 'w') as outfile:
    json.dump(data, outfile, sort_keys=True, indent=4, separators=(',', ': '))
