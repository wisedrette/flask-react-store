from flask import render_template, redirect, url_for, flash
from app import app
# from flask.ext.login import logout_user  # , login_required, current_user
from flask_security import login_required
from flask_security.utils import logout_user


@app.route('/')
def to_index():
    return redirect(url_for('index'))

@app.route('/socket.io/')
def socket_io():
    pass

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/index')
@app.route('/<path:anything>')
def index(anything=None):
    """
    Routes which are handled by React Router, thus there are matched <path:anything>
    :param anything:
    :return:
    """
    return render_template('index.html')

