import os
from werkzeug import secure_filename
from flask import request, flash
from flask_restful import Resource
from app import app, db, api, mail  # , socketio
from config import MAIL_USERNAME
from app.models import State, user_datastore, UserCodes, Discounts, id_generator
from flask.ext.mail import Message
from flask_security import current_user
from flask_security.utils import encrypt_password, verify_password, login_user


class PwdReset(Resource):
    @staticmethod
    def post(state):
        if state=='email':
            user = user_datastore.get_user(request.json.get('email'))
            if user is not None:
                code = id_generator()
                msg = Message('Password reset request',
                      sender=('BeerCraft support', MAIL_USERNAME), recipients=[user.email])
                msg.body = 'Here\'s the confirmation code you\'ll need to change your password:\n'+ code
                mail.send(msg)
                code = UserCodes(user_id=user.id, code=code)
                db.session.add(code)
                db.session.commit()
                return True
            else:
                return False
        elif state=='code':
            code = UserCodes.query.filter_by(code=request.json.get('code')).first()
            user = user_datastore.get_user(request.json.get('email'))
            if code is not None and code.user.email == request.json.get('email'):
                db.session.delete(code)
                for codes in UserCodes.query.filter_by(user_id=user.id).all():
                    db.session.delete(codes)
                db.session.commit()
                return True
            else:
                return False
        elif state=='pwd':
            user = user_datastore.get_user(request.json.get('email'))
            if user is not None:
                user.password = encrypt_password(request.json.get('password'))
                db.session.add(user)
                db.session.commit()
                login_user(user)
                flash('Password has been successfully reset')
                return True
            else:
                return False
        else:
            return False

class Login(Resource):
    @staticmethod
    def post():
        user = user_datastore.get_user(request.json.get('email'))
        if user is not None and verify_password(request.json.get('password'), user.password):
            login_user(user)
            flash('Welcome, ' + user.username + '!', 'ui')
            return {'login': True}
        else:
            return {'login': False}


class Account(Resource):
    @staticmethod
    def get():
        if current_user.is_authenticated:
            user = user_datastore.get_user(current_user.get_id())
            return {'user': user.serialize}
        else:
            return {'user': None}

    @staticmethod
    def post():
        user = user_datastore.get_user(current_user.get_id())
        for key in request.json.keys():
            setattr(user, key, request.json.get(key))
        db.session.add(user)
        db.session.commit()
        return {'message': ' saved'}


class EmailChange(Resource):
    @staticmethod
    def post():
        user = user_datastore.get_user(current_user.get_id())
        email_check = user_datastore.get_user(request.json.get('email').lower())
        if user is not None and email_check is None:
            user.email = request.json.get('email')
            db.session.add(user)
            db.session.commit()
            return {'saved': True}
        else:
            return {'saved': False}


class AvatarChange(Resource):
    @staticmethod
    def post():
        user = user_datastore.get_user(current_user.get_id())
        if user is not None and request.files['file']:
            img = request.files['file']
            filename = secure_filename(img.filename)
            img.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print filename
            user.avatar = '/static/img/avatars/' + filename
            db.session.add(user)
            db.session.commit()


class PasswordChange(Resource):
    @staticmethod
    def post():
        user = user_datastore.get_user(current_user.get_id())
        if user is not None and verify_password(request.json.get('old_password'), user.password):
            user.password = encrypt_password(request.json.get('password'))
            db.session.add(user)
            db.session.commit()
            return {'saved': True}
        else:
            return {'saved': False}


class Register(Resource):
    @staticmethod
    def post():
        name = user_datastore.find_user(username=request.json.get('username'))
        if name is None and user_datastore.get_user(request.json.get('email')) is None:
            user = user_datastore.create_user(
                username=request.json.get('username').lower(),
                password=encrypt_password(request.json.get('password')),
                email=request.json.get('email'),
                avatar='/static/placeholder.jpg',
                roles=[user_datastore.find_or_create_role('user')]
            )
            user_datastore.activate_user(user)
            db.session.commit()
            login_user(user)
            flash('Thanks for registering, ' + user.username + '!', 'ui')
            return {'register': True}
        else:
            return {'register': False}


class StatesList(Resource):
    @staticmethod
    def get():
        return {'states': [i.serialize for i in State.query.all()]}


class ContactInfo(Resource):
    @staticmethod
    def get():
        admin = user_datastore.get_user(1)

        if current_user.is_authenticated:
            user_data = user_datastore.get_user(current_user.get_id())
            user = {'username': user_data.username, 'email': user_data.email}
        else:
            user = {'username': '', 'email': ''}

        return {'contacts': {'address': admin.address,
                             'state': admin.state_code.code,
                             'zip': admin.zip,
                             'phone': admin.phone},
                'user': user
                }

    @staticmethod
    def post():
        msg = Message('Enquiry from ' + request.json.get('name') + ', email: ' + request.json.get('email'),
                      sender=('User enquiry', MAIL_USERNAME), recipients=[MAIL_USERNAME])
        msg.body = request.json.get('enquiry')
        mail.send(msg)
        flash('We\ve received a message from you. Thanks for your enquiry!', 'ui')


class SubmitDiscount(Resource):
    @staticmethod
    def post():
        code = Discounts.query.filter_by(code=request.json.get('code')).first()
        if code is not None:
            user = user_datastore.get_user(current_user.get_id())
            discount = code.discount
            user.discount = discount
            db.session.add(user)
            db.session.delete(code)
            db.session.commit()
            return {'state': True, 'discount': discount}
        else:
            return {'state': False}


api.add_resource(ContactInfo, '/api/contact_info')
api.add_resource(StatesList, '/api/states')
api.add_resource(Login, '/api/login')
api.add_resource(PwdReset, '/api/pwd_reset/<state>')
api.add_resource(Account, '/api/account')
api.add_resource(SubmitDiscount, '/api/account/submit_discount')
api.add_resource(EmailChange, '/api/account/email_change')
api.add_resource(AvatarChange, '/api/account/img_change')
api.add_resource(PasswordChange, '/api/account/pwd_change')
api.add_resource(Register, '/api/register')
