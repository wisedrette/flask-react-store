from uuid import uuid4
from flask import request, flash
from flask_restful import Resource
from app import db, api#, socketio
from app.models import Order, BeersInOrder, user_datastore, Beer
from flask_security import current_user
from datetime import datetime


class Checkout(Resource):
    @staticmethod
    def get():
        if current_user.is_authenticated:
            print Order.query.filter_by(user_id=current_user.get_id())

    @staticmethod
    def post():
        details = request.json.get('order')
        beers_list = details['beers']
        save_details = request.json.get('save')

        discount = request.json.get('discount')
        if discount != 0:
            user = user_datastore.get_user(current_user.get_id())
            user.discount = 0
            db.session.add(user)
            db.session.commit()


        details['uid'] = str(uuid4())
        details['uid'] = details['uid'][details['uid'].rfind('-')+1:].upper()
        details['uid'] = details['uid'][0:11]
        order = Order(user_id=current_user.get_id(), added_at=datetime.now(), delivered=False, discount=discount)
        db.session.add(order)
        db.session.commit()

        for beer in beers_list:
            relation = BeersInOrder(beer_id=beer['id'], order_id=order.id, count=beer['count'])
            beer_db = Beer.query.filter_by(id=beer['id']).first()
            beer_db.sold += beer['count']
            db.session.add(relation)
            db.session.add(beer_db)

        if current_user.is_authenticated:
            user = user_datastore.get_user(current_user.get_id())
        else:
            user = None

        for key in details:
            if key != 'beers':
                setattr(order, key, details[key])
                if save_details:
                    setattr(user, key, details[key])
        if save_details:
            db.session.add(user)

        db.session.commit()

        flash('Thanks for your order! We\'ll contact with you soon.', 'ui')

        # socketio.emit('post_order', {'order': order.serialize_admin})


class OrdersList(Resource):
    @staticmethod
    def get():
        query = Order.query.filter(Order.user_id.is_(current_user.get_id()))
        query = query.order_by(Order.added_at.desc())
        return {'orders': [i.serialize for i in query.all()]}


class UnconfirmedOrdersList(Resource):
    @staticmethod
    def get():
        query = Order.query.filter(Order.confirmed_at.is_(None))
        query = query.order_by(Order.added_at.desc())
        return {'orders': [i.serialize_admin for i in query.all()]}


api.add_resource(Checkout, '/api/checkout')
api.add_resource(UnconfirmedOrdersList, '/api/admin/orders')
api.add_resource(OrdersList, '/api/account/orders')
