from flask import request
from flask_restful import Resource
from app import api, db
from app.models import Beer, BeersInOrder, Type, Subcategory, Category, News, BeerUserRate, Review
from config import ENTRIES_PER_PAGE
from sqlalchemy import desc
from flask_security import current_user
from datetime import datetime


class CategoryHandler:
    """Class for handling change of content category"""

    def __init__(self, category_name):
        self.category_id = Category.query.filter(Category.name.like(category_name)).first().id

    def get_count(self):
        return Beer.query.filter(Beer.types.any(Type.id.in_(self.get_types_array()))).count()

    def get_types_array(self):
        return [i.id for i in Type.query.filter(Type.category_id == self.category_id).all()]

    def get_category_id(self):
        return self.category_id

    def get_serialized_types(self):
        return [i.serialize for i in Type.query.filter(Type.category_id == self.category_id).all()]

    def get_serialized_subcategories(self):
        return [i.serialize for i in Subcategory.query.filter(Subcategory.category_id == self.category_id).all()]


class Content(Resource):
    @staticmethod
    def post(category):
        page_num = request.json.get('page')
        if page_num is None:
            page_num = 1
        sort = request.json.get('sort')
        types = request.json.get('types')
        if types is None:
            types = []
        search = request.json.get('search')

        category_handler = CategoryHandler(category)
        query = Beer.query.filter(Beer.types.any(Type.id.in_(category_handler.get_types_array())))

        if sort == 'title':
            query = query.order_by(Beer.name)
        elif sort == 'date':
            query = query.order_by(desc(Beer.added))
        elif sort == 'price_up':
            query = query.order_by(Beer.price)
        elif sort == 'price_down':
            query = query.order_by(desc(Beer.price))
        elif sort == 'rating':
            query = query.order_by(desc(Beer.rate))
        else:
            query = query.order_by(desc(Beer.sold))

        if len(types) > 0:
            query = query.filter(Beer.types.any(Type.id.in_(types)))
        if len(search) > 0:
            query = query.filter(Beer.name.like('%' + search + '%'))

        paginate = query.paginate(page_num, ENTRIES_PER_PAGE, False)

        return {'data': [i.serialize for i in paginate.items],
                'pages': paginate.pages}


class IndexContent(Resource):
    @staticmethod
    def get(slug):
        if slug == 'news':
            news_list = News.query.order_by(desc(News.added)).limit(20)
            return {'news_list': [i.serialize_preview for i in news_list.all()]}
        else:
            query = Beer.query.filter_by(available=True)
            if slug == 'recent':
                query_resent_popular = query.filter(Beer.added >= datetime.strptime('10/04/2015', '%m/%d/%Y')).order_by(desc(Beer.rate)).limit(6)
                return {'data': [i.serialize_short for i in query_resent_popular.all()]}

            elif slug == 'bestsellers':
                query_sold = query.order_by(desc(Beer.sold)).limit(25)
                return {'data_bestselling': [i.serialize_short for i in query_sold.all()]}

            elif slug == 'new':
                query_new = query.order_by(desc(Beer.added)).limit(25)
                return {'data_date': [i.serialize_short for i in query_new.all()]}

            elif slug == 'rating':
                query_popular = query.order_by(desc(Beer.rate)).limit(25)
                return {'data_rating': [i.serialize_short for i in query_popular.all()]}


class NavigationInfo(Resource):
    @staticmethod
    def get(category):
        category_handler = CategoryHandler(category)

        return {'types': category_handler.get_serialized_types(),
                'categories': category_handler.get_serialized_subcategories(),
                'count': category_handler.get_count()}


class ProductInfo(Resource):
    @staticmethod
    def get(slug):
        product = Beer.query.filter((Beer.path.like('%' + slug))).first()
        types = Type.query.filter(Type.id.in_([i.id for i in product.types])).all()
        votes = BeerUserRate.query.filter_by(beer_id=product.id).count()
        reviews = Review.query.filter_by(beer_id=product.id).order_by(desc(Review.added_at)).all()
        if current_user.is_authenticated:
            vote = BeerUserRate.query.filter_by(user_id=current_user.get_id()).filter_by(beer_id=product.id).first()
            if vote is not None:
                vote = vote.rate
        else:
            vote = None
        return {'product': product.serialize, 'types': [i.name for i in types], 'votes': votes, 'vote': vote,
                'reviews': [i.serialize for i in reviews]}


class NewsInfo(Resource):
    @staticmethod
    def get(slug):
        news = News.query.filter(News.path.like('%' + slug)).first()
        headlines = News.query.filter(News.id != news.id).order_by(desc(News.added)).limit(8)
        prev_n = News.query.filter(News.added < news.added).order_by(desc(News.added)).first()
        next_n = News.query.filter(News.added > news.added).order_by(News.added).first()
        if prev_n is not None:
            prev_n = prev_n.serialize_short
        if next_n is not None:
            next_n = next_n.serialize_short

        return {'news': news.serialize, 'headlines': [i.serialize_short for i in headlines],
                'prev': prev_n, 'next': next_n}


class RateProduct(Resource):
    @staticmethod
    def post(prod_id):
        rate = BeerUserRate(beer_id=prod_id, rate=int(request.json.get('rate')), user_id=current_user.get_id())
        db.session.add(rate)
        db.session.commit()


class AddReview(Resource):
    @staticmethod
    def post(prod_id):
        review = Review(beer_id=prod_id, user_id=current_user.get_id(), added_at=datetime.now(),
                        title=request.json.get('title'), text=request.json.get('text'))
        db.session.add(review)
        db.session.commit()


class SimilarProducts(Resource):
    @staticmethod
    def get_orders_relation(prod_list):
        orders_id = BeersInOrder.query.filter(BeersInOrder.beer_id.in_(prod_list)).all()
        orders_id = list(set([i.order_id for i in orders_id]))
        if len(orders_id) > 0:
            beers = BeersInOrder.query.filter(BeersInOrder.order_id.in_(orders_id))
            beers = beers.filter(~(BeersInOrder.beer_id.in_(prod_list)))

            beers = beers.group_by(BeersInOrder.beer_id)
            for beer in beers:
                beer_counts = BeersInOrder.query.filter_by(beer_id=beer.beer_id).all()
                counts = [beer.count for beer in beer_counts]
                beer.count = sum(counts)

            return beers.order_by(desc(BeersInOrder.count))

    @staticmethod
    def get_types_relation(prod_list):
        beers = Beer.query.filter(Beer.id.in_(prod_list)).all()
        types = []
        for beer in beers:
            for i in beer.types:
                types.append(i.id)
        query = Beer.query.filter(Beer.types.any(Type.id.in_(types)))
        query = query.filter(~(Beer.id.in_(prod_list)))
        return query.order_by(desc(Beer.sold))

    def get(self, relation, prod_id=None, prod_ids=None, page_num=1):

        if prod_id is not None:
            beers_id = [prod_id]
        elif prod_ids is not None:
            beers_id = [int(x) for x in prod_ids.split(',')]
        else:
            return

        if relation == 'orders' and prod_id is not None:
            query = self.get_orders_relation(beers_id)
            if query is not None and len(query.all()) > 0:
                query = query.paginate(page_num, 5, False)
                return {'data_o': [i.beer.serialize_short for i in query.items], 'pages': query.pages}
            else:
                return {'data_o': []}
        else:
            query = self.get_types_relation(beers_id)
            if prod_id is not None:
                query = query.paginate(page_num, 5, False)
                return {'data_s': [i.serialize_short for i in query.items], 'pages': query.pages}
            else:
                query = query.limit(12)
                return {'data': [i.serialize_short for i in query.all()]}


api.add_resource(Content, '/api/content/<path:category>')
api.add_resource(ProductInfo, '/api/product/<path:slug>')
api.add_resource(NewsInfo, '/api/news/<path:slug>')
api.add_resource(SimilarProducts, '/api/similar/<path:relation>/<int:prod_id>',
                 '/api/similar/<path:relation>/<path:prod_ids>',
                 '/api/similar/<path:relation>/<int:prod_id>/<int:page_num>')
api.add_resource(RateProduct, '/api/rate/<int:prod_id>')
api.add_resource(IndexContent, '/api/index_content/<path:slug>')
api.add_resource(NavigationInfo, '/api/navigation/<path:category>')
api.add_resource(AddReview, '/api/review/<int:prod_id>')
