from flask import redirect, url_for, request, abort
from app import app, db, socketio
from app.models import Beer, Type, User, Order, BeersInOrder, Subcategory, Category, News, Review, Discounts, user_datastore
from config import file_path
from flask_security import Security, current_user
from flask_security.utils import encrypt_password
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin, form, AdminIndexView, expose
from flask_admin import helpers as admin_helpers
from jinja2 import Markup
from datetime import datetime


class MyHomeView(AdminIndexView):
    """
        Override builtin AdminIndexView for index page in order to redirect users when a view is not accessible.
    """

    @expose('/')
    def index(self):
        if User.query.count() == 0:
            admin_user = user_datastore.create_user(
                username='admin',
                password=encrypt_password('admin'),
                email='admin@beercraft.com',
                avatar='/static/placeholder.jpg',
                address='2120 S Michigan Ave, Chicago',
                state='IL',
                zip='60616',
                phone='626-244-4462',
                roles=[user_datastore.find_or_create_role('user'),
                       user_datastore.find_or_create_role('superuser')]
            )
            db.session.add(admin_user)
            db.session.commit()
        if not current_user.is_authenticated:
            return redirect(url_for('security.login', next=request.url))
        elif not current_user.has_role('superuser'):
            abort(403)
        else:
            return self.render(self._template)


admin = Admin(app, name='BeerCraft store', template_mode='bootstrap3', index_view=MyHomeView())
security = Security(app, user_datastore)  # , login_form=ExtendedLoginForm)


class MyModelView(ModelView):
    column_default_sort = ('id', True)
    page_size = 25

    @staticmethod
    def is_accessible():
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True

            # return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))


class UserView(MyModelView):
    column_list = ['username', 'email', 'address', 'phone', 'zip', 'state', 'active', 'roles']
    column_labels = dict(state_code='State')
    form_excluded_columns = ['password', 'avatar']
    column_sortable_list = ('username', 'email', 'state')


class BeerView(MyModelView):
    def _list_thumbnail(view, context, model, name):
        if not model.img_path:
            return ''

        return Markup('<img src="%s" width=100 height=100>' % url_for('static',
                                                                      filename=model.img_path))

    column_formatters = {
        'img_path': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'img_path': form.ImageUploadField('Image',
                                          base_path=file_path, relative_path='img/products/')
    }

    column_list = ['name', 'price', 'sold', 'img_path', 'types', 'rate', 'available', 'added']
    column_labels = dict(img_path='Image')
    column_sortable_list = ('price', 'name', ('types', 'types.name'), 'added', 'sold', 'rate')
    column_searchable_list = ['name', 'types.name']

    column_editable_list = ['name', 'price', 'available', 'rate']

    form_excluded_columns = ['orders']

    form_overrides = {
        'img_path': form.ImageUploadField
    }

    column_display_pk = True


class NewsView(MyModelView):
    column_default_sort = ('added', True)

    def _list_thumbnail(view, context, model, name):
        if not model.img_path:
            return ''

        return Markup('<img src="%s" width=100 height=100>' % url_for('static',
                                                                      filename=model.img_path))

    column_formatters = {
        'img_path': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'img_path': form.ImageUploadField('Image',
                                          base_path=file_path, relative_path='img/news/')
    }

    column_exclude_list = ['text', 'path']
    column_labels = dict(img_path='Image')
    column_sortable_list = ('title', 'added')

    column_editable_list = ['title', 'added']
    form_overrides = {
        'img_path': form.ImageUploadField
    }
    column_display_pk = True


class OrderView(MyModelView):
    def disabled_fields(*args):
        fields = {}
        for i in args:
            fields[i] = {
                'disabled': True
            }
        return fields

    column_display_pk = True
    form_widget_args = disabled_fields('added_at', 'uid', 'total')


class DiscountView(MyModelView):
    def disabled_fields(*args):
        fields = {}
        for i in args:
            fields[i] = {
                'disabled': True
            }
        return fields

    column_display_pk = True
    form_widget_args = disabled_fields('code')

class BeersInOrderView(MyModelView):
    column_searchable_list = ['order_id']


class TypeView(MyModelView):
    column_sortable_list = ('name', ('subcategory', 'subcategory.name'), ('category', 'category.name'))
    column_editable_list = ('subcategory', 'category')
    column_list = ('name', 'subcategory', 'category')


admin.add_view(UserView(User, db.session))
admin.add_view(NewsView(News, db.session))
admin.add_view(OrderView(Order, db.session))
admin.add_view(BeersInOrderView(BeersInOrder, db.session))
admin.add_view(BeerView(Beer, db.session))
admin.add_view(TypeView(Type, db.session))
admin.add_view(MyModelView(Subcategory, db.session))
admin.add_view(MyModelView(Category, db.session))
admin.add_view(MyModelView(Review, db.session))
admin.add_view(DiscountView(Discounts, db.session))


# @socketio.on('connect')
# def test_connect():
#     print 'user connected'
#
#
# @socketio.on('confirm_order')
# def handle_confirm(order_id):
#     order = Order.query.filter(Order.id == order_id).first()
#     order.confirmed_at = datetime.now()
#     db.session.add(order)
#     db.session.commit()


# define a context processor for merging flask-admin's template context into the
# flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
    )
