import React from 'react'
import { Panel } from 'react-bootstrap';
import * as texts from './text.js'
import { FormattedMessage } from 'react-intl';
import { messages } from '../helper_functions/intlMessages.js'
import { connect } from 'react-redux'


class InfoPages extends React.Component {
    render() {
        var text = texts[this.props.params.page_name];

        var panelText = text['text_'+this.props.locale].map((paragraph, i) => <p key={i}>{paragraph}</p>);
        return (
            <div className='content'>
                <h3 className='header'><FormattedMessage {...messages[text.title]} /></h3>
                <Panel>
                    {panelText}
                </Panel>
            </div>
        );
    }
}

export default connect(
    // Connect React Intl with Redux to inject current locale data
    ({ intl }) => ({...intl, key: intl.locale})
)(InfoPages)