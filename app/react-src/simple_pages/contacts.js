import React from 'react'
import { ButtonInput } from 'react-bootstrap'
import { InputField } from '../components/shared_components'
import addMap from '../helper_functions/addMap'
import { FormattedMessage } from 'react-intl';
import { messages } from '../helper_functions/intlMessages.js'

export default class Contacts extends React.Component {
    constructor() {
        super();
        this.state = {
            contacts: {address: '', state: '', zip: '', phone: ''},
            user: {username: '', email: ''}
        }
    }

    ajaxRequestContacts() {
        $.ajax({
            url: '/api/contact_info',
            method: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            success: (function (data) {
                this.setState(data);
                addMap(data.contacts);
            }.bind(this))
        });
    }

    componentDidMount() {
        this.ajaxRequestContacts();
    }

    render() {
        return (
            <div className='content contacts'>
                <h3 className='text-center'><FormattedMessage {...messages.contact} /></h3>
                <hr />
                <section className='location'>
                    <p><FormattedMessage {...messages.location} /></p>
                    <div id='map' className='map'></div>
                </section>
                <ContactForm username={this.state.user.username} email={this.state.user.email}/>
            </div>
        );
    }
}


class ContactForm extends React.Component {
    constructor() {
        super();
    }

    serialize() {
        return JSON.stringify({
            name: this.refs.name.getValue(),
            email: this.refs.email.getValue(),
            enquiry: this.refs.enquiry.getValue()
        })
    }

    submitEnquiry(e) {
        var passed = true;
        Object.keys(this.refs).forEach(key => passed = this.refs[key].validate() && passed);

        if (passed) {
            $.ajax({
                url: '/api/contact_info',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: this.serialize(),
                beforeSend: ( () => Object.keys(this.refs).forEach(key => this.refs[key].resetValue()) ),
                success: (() => location.reload())
            });
        }
        e.target.blur();
    }

    render() {
        return (
            <section className='contact-form'>
                <InputField type='text' label={<FormattedMessage {...messages.your_name} />} placeholder='Enter your name' value={this.props.username}
                            ref='name'/>
                <InputField type='text' label='Email' placeholder='Enter your email' value={this.props.email}
                            dataType='email' ref='email'/>
                <InputField type='textarea' label={<FormattedMessage {...messages.enquiry} />}
                            placeholder='Enter your message'
                            ref='enquiry'/>
                <ButtonInput type='submit' value='Submit' onClick={(e) => this.submitEnquiry(e)}/>
            </section>
        )
    }
}
