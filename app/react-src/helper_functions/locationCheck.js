function resultsFilter(results, type, short_name) {
    if (short_name == undefined)
        short_name = '.*';
    var regexp = new RegExp(short_name);
    return results.filter(result => result.address_components.filter(
            field => field.types[0] == type && regexp.test(field.short_name)
    ).length != 0);
}

function getFieldValue(results, name) {
    return results[0].address_components.filter(states => states.types[0] == name)[0].short_name;
}

export default function locationCheck(state, address, zip, processResult) {
    var geoCoder = new google.maps.Geocoder();

    var searchAddress = address == '' ? state : address;

    geoCoder.geocode({address: searchAddress}, function (results, status) {
        // filter addresses which are located in USA
        results = resultsFilter(results, 'country', 'US');

        // if state is specified, filter by state value
        if (state != '' && state != 0){
            var subResults = resultsFilter(results, 'administrative_area_level_1', state);
            if (subResults.length == 0) {
                processResult(false, {message: 'Address cannot be located in this state'});
                return;
            }
            else
                results = subResults;
        }

        if (status == google.maps.GeocoderStatus.OK) {
            if (results.length != 0) {
                if (results.filter(result => result.address_components[0].types[0] != 'locality') == 0)
                    processResult(false, {message: 'Please specify full address'});
                else {
                    // remove USA from address string
                    searchAddress = results[0].formatted_address.replace(', USA', '');

                    // remove state from address string
                    searchAddress = searchAddress.slice(0, searchAddress.lastIndexOf(','));

                    //get zip code and state from search results
                    var searchZip = getFieldValue(results, 'postal_code');
                    var searchState = getFieldValue(results, 'administrative_area_level_1');


                    if (searchAddress.toLowerCase() != address.toLowerCase() || state != searchState || zip != searchZip) {
                        address = searchAddress;
                        state = searchState;
                        zip = searchZip;
                        processResult(true, {
                            message: 'Address has been autocompleted, please recheck it',
                            result: {address: address, state: state, zip: zip}
                        });
                    } else
                        processResult(true, {
                            message: '',
                            result: {address: address, state: state, zip: zip}
                        });
                }

            } else {
                processResult(false, {message: 'Address cannot be located in USA'});
            }
        } else {
            processResult(false, {message: 'Address cannot be located in USA'});
        }
    });
}
