import { defineMessages } from 'react-intl'


export const messages = defineMessages({
    headlines: {
        id: 'headlines', defaultMessage: 'NEWS HEADLINES'
    },
    description: {
        id: 'description', defaultMessage: 'description'
    },
    media: {
        id: 'media', defaultMessage: 'media'
    },
    reviews: {
        id: 'reviews', defaultMessage: 'User reviews'
    },
    av_rating: {
        id: 'av_rating', defaultMessage: 'Average rating'
    },
    my_rating: {
        id: 'my_rating', defaultMessage: 'My rating'
    },
    my_review: {
        id: 'my_review', defaultMessage: 'My review'
    },
    add_review: {
        id: 'add_review', defaultMessage: 'Add now'
    },
    more_recommendations: {
        id: 'more_recommendations', defaultMessage: 'More recommendations'
    },
    no_reviews: {
        id: 'no_reviews', defaultMessage: 'There are no reviews yet.'
    },
    also_bought: {
        id: 'also_bought', defaultMessage: 'People who bought it also bought'
    },
    also_like: {
        id: 'also_like', defaultMessage: 'You might also like'
    },
    buy: {
        id: 'buy', defaultMessage: 'Buy item'
    },
    add_to_cart: {
        id: 'add_to_cart', defaultMessage: 'Add to cart'
    },
    checkout: {
        id: 'checkout', defaultMessage: 'Checkout'
    },
    beers: {
        id: 'beers', defaultMessage: 'beers'
    },
    sodas: {
        id: 'sodas', defaultMessage: 'crafted sodas'
    },
    shipping: {
        id: 'shipping', defaultMessage: 'shipping'
    },
    contact: {
        id: 'contact', defaultMessage: 'Contact us'
    },
    sale: {
        id: 'sale', defaultMessage: 'ON SALE'
    },
    bestsellers: {
        id: 'bestsellers', defaultMessage: 'BESTSELLERS'
    },
    newest: {
        id: 'newest', defaultMessage: 'Newest products'
    },
    rated: {
        id: 'rated', defaultMessage: 'Top rated'
    },
    info: {
        id: 'info', defaultMessage: 'Information'
    },
    ship_info: {
        id: 'ship_info', defaultMessage: 'Shipping information'
    },
    policy: {
        id: 'policy', defaultMessage: 'Privacy policy'
    },
    terms: {
        id: 'terms', defaultMessage: 'Terms&Conditions'
    },
    account: {
        id: 'account', defaultMessage: 'Account'
    },
    history: {
        id: 'history', defaultMessage: 'Orders History'
    },
    settings: {
        id: 'settings', defaultMessage: 'Profile settings'
    },
    discounts: {
        id: 'discounts', defaultMessage: 'Discounts'
    },
    redeem: {
        id: 'redeem', defaultMessage: 'Redeem discount code'
    },
    brewery_submit: {
        id: 'brewery_submit', defaultMessage: 'Submit brewery product'
    },
    lang: {
        id: 'lang', defaultMessage: 'Language'
    },
    curr: {
        id: 'curr', defaultMessage: 'Currency'
    },
    location: {
        id: 'location', defaultMessage: 'Our location'
    },
    enquiry: {
        id: 'enquiry', defaultMessage: 'Enquiry'
    },
    your_name: {
        id: 'yname', defaultMessage: 'Your name'
    },
    signup: {
        id: 'signup', defaultMessage: 'Sign up'
    },
    signout: {
        id: 'signout', defaultMessage: 'Sign out'
    },
    login: {
        id: 'login', defaultMessage: 'Login'
    },
    headline: {
        id: 'headline', defaultMessage: 'headlines'
    },
    what_new: {
        id: 'what_new', defaultMessage: 'What\'s new'
    },
    deals: {
        id: 'deals', defaultMessage: 'Latest deals'
    },
    why_here: {
        id: 'why_here', defaultMessage: 'Why shop here?'
    },
    popular: {
        id: 'popular', defaultMessage: 'Popular'
    },
    new: {
        id: 'new', defaultMessage: 'New'
    },
    more_news: {
        id: 'more_news', defaultMessage: 'More news'
    },
    more_popular: {
        id: 'more_popular', defaultMessage: 'More popular'
    },
    more_new: {
        id: 'more_new', defaultMessage: 'More new'
    },
    more_rated: {
        id: 'more_rated', defaultMessage: 'More top rated'
    },
    free_shipping: {
        id: 'free_shipping', defaultMessage: 'Free Shipping'
    },
    no_tax: {
        id: 'no_tax', defaultMessage: 'No Sales Tax'
    },
    destination_check: {
        id: 'destination_check', defaultMessage: 'Destination Check'
    },
    free_shipping_text: {
        id: 'free_shipping_text',
        defaultMessage: '<p>We offer free shipping to the continental United States for qualifying packages.</p> Nulla eget urna vel diam dui.Auctor tellus aliquam dolor erat vel.Eget praesent veniam.Autem eu elit mattis qui hendrerit.Eget id egestas duis nunc wisi vehicula sed dui duis tincidunt suspendisse libero mus metus.'
    },
    no_tax_text: {
        id: 'no_tax_text',
        defaultMessage: '<p>We don\'t charge any sales tax for orders shipped outside of California. </p> Eleifend ligula ornare.Sit magna dolor.Ipsum sem blandit.Suspendisse sit maecenas ante aliquam blandit et tempus et in turpis accumsan.Egestas mauris nullam.Nunc pellentesque integer lectus augue dictum augue sed id est nulla eu.'
    },
    destination_check_text: {
        id: 'destination_check_text',
        defaultMessage: '<p>We check the weather forecast of each order\'s destination before we ship it. </p> Morbi vulputate quis sit enim posuere fringilla nibh molestie lobortis nec nec pede molestie neque.Ac leo dolor justo sociosqu faucibus.Nam con elementum pede tristique sit.Lorem vel metus.'
    },
    clear_filters: {
        id: 'clear_filters', defaultMessage: ' CLEAR FILTERS'
    },
    order: {
        id: 'order', defaultMessage: 'Order: '
    },
    view: {
        id: 'view', defaultMessage: 'View: '
    },
    list: {
        id: 'list', defaultMessage: 'as list'
    },
    grid: {
        id: 'grid', defaultMessage: 'as grid'
    },
    price: {
        id: 'price', defaultMessage: 'by price '
    },
    title: {
        id: 'title', defaultMessage: 'by title'
    },
    bestselling: {
        id: 'bestselling', defaultMessage: 'by bestselling'
    },
    rating: {
        id: 'rating', defaultMessage: 'by user rating'
    },
    date: {
        id: 'date', defaultMessage: 'by date added'
    }
});

export const ruMessages = defineMessages({
    headlines: 'другие новости', description: 'описание', media: 'галерея', reviews: 'Отзывы пользователей',
    av_rating: 'Средняя оценка', my_rating: 'Моя оценка', my_review: 'Мой отзыв', add_review: 'добавить',
    beers: 'пиво', sodas: 'газировка', shipping: 'доставка', contact: 'Контакты', sale: 'СО СКИДКОЙ',
    add_to_cart: 'Добавить в корзину', checkout: 'Оформить заказ',
    more_recommendations: 'Больше рекоммендаций', also_bought: 'купившие это также приобрели', buy: 'Купить продукт',
    no_reviews: 'Ещё нет отзывов.', also_like: 'вам также может понравиться', bestsellers: 'ЛИДЕРЫ ПРОДАЖ', lang: 'Язык',
    newest: 'Свежие поставки', rated: 'Высоко оцененные', info: 'Информация', ship_info: 'Информация о доставке',
    policy: 'Правила', terms: 'Условия', account: 'Аккаунт', history: 'История заказов', settings: 'Настройки',
    discounts: 'Скидки', redeem: 'Активировать скидочный код', brewery_submit: 'Предложить продукт', curr: 'Валюта',
    location: 'Наше месторасположение', enquiry: 'Вопрос', your_name: 'Ваше имя', signup: 'Регистрация',
    signout: 'Выход', login: 'Войти', why_here: 'Почему стоит выбрать нас?', what_new: 'Новинки', headline: 'Новости',
    popular: 'Популярные', new: 'Новые', more_news: 'Больше новостей', more_popular: 'Больше популярных',
    more_new: 'Больше новых', more_rated: 'Больше высоко оцененных', deals: 'Актуальные скидки',
    free_shipping: 'Бесплатная доставка', no_tax: 'Без налогов с продаж', destination_check: 'Проверка адреса доставки',
    free_shipping_text: '<p>Мы предлагаем бесплатную доставку в континентальной части Соединенных Штатов для отборочных посылок.</p> Нулла егет урна вел диам дуи.Ауцтор теллус алияуам долор ерат вел.Егет праесент вениам.Аутем еу елит маттис яуи хендрерит.Егет ид егестас дуис нунц щиси вехицула сед дуи дуис тинцидунт суспендиссе либеро мус метус.',
    no_tax_text: '<p>Мы не взимаем налогов с продаж для заказов, адрес доставки которых вне штата Калифорния. </p> Елеифенд лигула орнаре.Сит магна долор.Ипсум сем бландит.Суспендиссе сит маеценас анте алияуам бландит ет темпус ет ин турпис аццумсан.Егестас маурис нуллам.Нунц пеллентесяуе интегер лецтус аугуе дицтум аугуе сед ид ест нулла еу.',
    destination_check_text: '<p>Мы проверяем прогноз погоды адреса каждого получателя перед тем, как отправляем доставку. </p> Морби вулпутате яуис сит еним посуере фрингилла нибх молестие лобортис нец нец педе молестие неяуе.Ац лео долор юсто социосяу фауцибус.Нам цон елементум педе тристияуе сит.Лорем вел метус.',
    order: 'Сортировать: ', view: 'Вид: ', list: 'списком', grid: 'плиткой', price: 'по цене ', title: 'по названию',
    bestselling: 'по популярности', rating: 'по рейтингу', date: 'по дате добавления', clear_filters: ' ОЧИСТИТЬ ФИЛЬТРЫ'
});