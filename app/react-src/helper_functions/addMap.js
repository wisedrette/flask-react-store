export default function addMap(addressData){
        var address = addressData.address + ' ' + addressData.state;
        var initPosition = {lat: 41.853593, lng: -87.623977};
        var contentPosition = {lat: 41.854268, lng: -87.624221};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: initPosition
        });
        var geocoder = new google.maps.Geocoder();


        var content = '<div><strong>Some Address Here</strong><br>' + address + '<br>' + '<strong>Phone:</strong> ' + addressData.phone + '</div>';
        var infoWindow = new google.maps.InfoWindow({
            content: content,
            position: contentPosition
        });

        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var position = results[0].geometry.location;

                map.setCenter(position);

                var marker = new google.maps.Marker({
                    map: map,
                    position: position
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open(map, this);
                });

                google.maps.event.trigger(marker, 'click');
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
}

