import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, Link, browserHistory } from 'react-router'
import jQuery from 'jquery';

import App from './app.js'
import { InfoPages, Contacts } from './simple_pages/'
import { Account, Profile, Discounts, OrdersHistory } from './components/account/'
import { Content, IndexPage, Product, News } from './components/content/'
import Cart from './components/cart_widget_checkout/shopping-cart.js'

import { createStore } from 'redux'
import { combineReducers } from 'redux';
import { Provider, intlReducer } from 'react-intl-redux'
import { beerList, filter } from './redux/reducers/'

const reducer = combineReducers({intl: intlReducer, beerList, filter, messages});
const store = createStore(reducer);
import {IntlProvider} from 'react-intl';


render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path='/' component={App}>
                <IndexRoute component={IndexPage}/>
                <Route path='index' component={IndexPage}/>
                <Route path='contact' component={Contacts}/>
                <Route path='info/:page_name' component={InfoPages}/>
                <Route path='checkout' component={Cart}/>
                <Route path='beers' name='beers' component={Content}/>
                <Route path='sodas' name='sodas' component={Content}/>
                <Route path='(beers)(sodas)/:name' component={Product}/>
                <Route path='news/:name' component={News}/>

                <Route path='account' component={Account}>
                    <Route path='settings' component={Profile}/>
                    <Route path='orders' component={OrdersHistory}/>
                    <Route path='discounts' component={Discounts}/>
                </Route>
            </Route>

        </Router>
    </Provider>,

    document.getElementById('mount-point'));