import * as types from '../actions/actionBeerTypes.js';

const initialState = {
    beers: JSON.parse(localStorage.getItem('cartList')) || []
};


var containsBeerObject = function (array, obj) {
    return array.filter(item => item.id == obj.id).length > 0;
};

export default function beers(state, action) {
    if (state === undefined) state = initialState;
    var beerList = state.beers;
    switch (action.type) {
        case types.ADD_BEER:
            if (containsBeerObject(beerList, action.beer))
                beerList.forEach(function (item) {
                    if (item.id == action.beer.id)
                        item.count += 1
                });
            else
                beerList.push(action.beer);
            localStorage.setItem('cartList', JSON.stringify(beerList));
            return {
                ...state,
                beers: beerList
            };


        case types.DELETE_BEER:
            beerList = state.beers.filter(function (item) {
                if (item.id == action.id && item.count > 1) {
                    item.count += -1;
                    return true
                }
                return item.id != action.id
            });
            localStorage.setItem('cartList', JSON.stringify(beerList));
            return {
                ...state,
                beers: beerList
            };


        case types.DELETE_BEERS:
            beerList = beerList.filter(item => item.id != action.id);
            localStorage.setItem('cartList', JSON.stringify(beerList));
            return {
                ...state,
                beers: beerList
            };

        case types.CLEAR_CART:
            beerList = [];
            localStorage.setItem('cartList', JSON.stringify(beerList));
            return {
                ...state,
                beers: beerList
            };

        default:
            return state;
    }
}