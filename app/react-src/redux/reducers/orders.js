import * as types from '../actions/actionAdminTypes.js';


const initialState = {
    orders: []
};


export default function orders(state, action) {
    if (state === undefined) state = initialState;
    var orderList = state.orders;
    switch (action.type) {
        case types.ADD_NEW_ORDER:
                orderList.push(action.order);
            return {
                ...state,
                orders: orderList
            };

        case types.CONFIRM_ORDER:
            orderList = state.orders.filter(item => item.id != action.id);
            return {
                ...state,
                orders: orderList
            };


        default:
            return state;
    }
}