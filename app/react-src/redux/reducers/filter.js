import * as types from '../actions/actionFilterTypes.js';
import { browserHistory } from 'react-router'

function getInitialFilter(reset) {
    const initialFilter = {
        types: [],
        sort: 'bestselling',
        search: '',
        page: 1,
        view: localStorage.getItem('view') || 'grid'
    };
    var query = window.location.search.substring(1);

    if (query.length == 0 || reset)
        return {filter: initialFilter};

    else {
        var filter = initialFilter;
        query = query.split('&');
        query.forEach(function (subquery) {
            subquery = subquery.split('=');
            switch (subquery[0]) {
                case 'types':
                    subquery[1] = JSON.parse('[' + subquery[1] + ']');
                    break;
                case 'page':
                    subquery[1] = parseInt(subquery[1]);
                    break;
                case 'search':
                    subquery[1] = decodeURI(subquery[1]);
            }
            filter[subquery[0]] = subquery[1]
        });

        return {filter: filter}
    }
}

function updateHistory(filter, actionKey) {
    var url = window.location.pathname + '?';
    var query = window.location.search.substring(1);

    var keys = Object.keys(filter);
    keys.splice(keys.indexOf('view'), 1);
    keys = keys.filter(key => query.indexOf(key) != -1 || key == actionKey || key == 'page');

    if (/search|types/.test(actionKey) && filter[actionKey].length == 0)
        keys.splice(keys.indexOf(actionKey), 1);

    keys.forEach(function (key, i) {
        if (i != keys.length - 1)
            url += key + '=' + filter[key] + '&';
        else {
            url += key + '=' + filter[key];
        }
    });

    browserHistory.push(url);
}


export default function filters(state, action) {
    if (state === undefined) state = getInitialFilter(false);
    var filter = state.filter;
    switch (action.type) {
        case types.SELECT_PAGE:
            filter.page = action.id;

            updateHistory(filter, 'page');
            return {
                ...state,
                filter: filter
            };


        case types.SELECT_TYPE:
            if (filter.types.indexOf(action.id) == -1)
                filter.types.push(action.id);
            else
                filter.types.splice(filter.types.indexOf(action.id), 1);
            filter.page = 1;

            updateHistory(filter, 'types');
            return {
                ...state,
                filter: filter
            };

        case types.UPDATE_TYPES:
            filter.types = action.types;

            updateHistory(filter, 'types');
            return {
                ...state,
                filter: filter
            };


        case types.SELECT_SORT:
            filter.sort = action.sortType;
            filter.page = 1;
            updateHistory(filter, 'sort');
            return {
                ...state,
                filter: filter
            };

        case types.SELECT_VIEW:
            filter.view = action.viewType;
            localStorage.setItem('view', action.viewType);

            filter.page = 1;
            return {
                ...state,
                filter: filter
            };

        case types.SEARCH_QUERY:
            filter.search = action.query;
            filter.page = 1;
            updateHistory(filter, 'search');
            return {
                ...state,
                filter: filter
            };

        case types.CLEAR_FILTER:
            var cleared = getInitialFilter(true);
            cleared.filter.view = 'grid';

            localStorage.setItem('view', cleared.filter.view);
            filter = cleared.filter;
            return {
                ...state,
                filter: filter
            };

        case types.UPDATE_FILTER:
            var updated = getInitialFilter(false);
            filter = updated.filter;
            return {
                ...state,
                filter: filter
            };

        default:
            return state;
    }
}