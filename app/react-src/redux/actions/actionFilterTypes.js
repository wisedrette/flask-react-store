export const CLEAR_FILTER = 'CLEAR_FILTER';
export const UPDATE_TYPES = 'UPDATE_TYPES';
export const UPDATE_FILTER = 'UPDATE_FILTER';

export const SELECT_PAGE = 'SELECT_PAGE';
export const SELECT_SORT = 'SELECT_SORT';
export const SELECT_TYPE = 'SELECT_TYPE';
export const SELECT_VIEW = 'SELECT_VIEW';
export const SEARCH_QUERY = 'SEARCH_QUERY';