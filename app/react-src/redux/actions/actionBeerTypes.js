export const ADD_BEER = 'ADD_BEER';
export const DELETE_BEER = 'DELETE_BEER';
export const DELETE_BEERS = 'DELETE_BEERS';
export const CLEAR_CART = 'CLEAR_CART';
