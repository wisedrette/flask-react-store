import * as beerTypes from './actionBeerTypes.js';
import * as adminTypes from './actionAdminTypes.js';
import * as filterTypes from './actionFilterTypes.js';

/** Actions related to shopping cart **/

export function addBeer(beer) {
    return {
        type: beerTypes.ADD_BEER,
        beer
    };
}

export function deleteBeer(id) {
    return {
        type: beerTypes.DELETE_BEER,
        id
    };
}

export function deleteBeers(id) {
    return {
        type: beerTypes.DELETE_BEERS,
        id
    }
}

export function clearCart() {
    return {
        type: beerTypes.CLEAR_CART
    }
}


/** Actions related to filtering **/

export function selectType(id) {
    return {
        type: filterTypes.SELECT_TYPE,
        id
    };
}

export function selectPage(id) {
    return {
        type: filterTypes.SELECT_PAGE,
        id
    };
}

export function selectView(viewType) {
    return {
        type: filterTypes.SELECT_VIEW,
        viewType
    };
}

export function selectOrder(sortType) {
    return {
        type: filterTypes.SELECT_SORT,
        sortType
    };
}

export function searchQuery(query) {
    return {
        type: filterTypes.SEARCH_QUERY,
        query
    };
}

export function clearFilter() {
    return {
        type: filterTypes.CLEAR_FILTER
    }
}

export function updateFilter() {
    return {
        type: filterTypes.UPDATE_FILTER
    }
}

export function updateTypes(types) {
    return {
        type: filterTypes.UPDATE_TYPES,
        types
    }
}

/** Actions related to admin page **/
export function addNewOrder(order) {
    return {
        type: adminTypes.ADD_NEW_ORDER,
        order
    }
}

export function confirmOrder(id) {
    return {
        type: adminTypes.CONFIRM_ORDER,
        id
    }
}