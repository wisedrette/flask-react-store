import React from 'react';
import {  Navbar, NavItem, NavDropdown } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import CartWidget from '../cart_widget_checkout/shopping-cart-widget.js'
import { LoginModal, RegisterModal, ResetModal } from './modals'
import LinkWrapper from '../shared_components/linkWrapper'
import { FormattedMessage } from 'react-intl';
import { messages } from '../../helper_functions/intlMessages.js'


export default class NavbarMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showLoginModal: false, showRegisterModal: false, showResetModal: false};
        this.toggleCollapsedNavbar = this.toggleCollapsedNavbar.bind(this);
        this.switchModals = this.switchModals.bind(this);
    }

    toggleCollapsedNavbar() {
        var contentWrapper = $('.content-wrapper');
        if (/widget-expanded/.test(contentWrapper.attr('class'))) {
            $('.navbar-head .dropdown-cart.collapsed').removeClass('is-expanded').addClass('is-contracted')
                .find('.dropdown-container').removeClass('in');
            contentWrapper.removeClass('widget-expanded');
        }
        contentWrapper.toggleClass('navbar-expanded');
        $(this.refs.collapsible).toggleClass('is-expanded is-contracted');
    }

    switchModals(modal) {
        var newState = {};
        Object.keys(this.state).forEach(key => newState[key] = key == modal);
        this.setState(newState);
    }

    navbarLinkClickHandler(){
        if (/widget-expanded/.test($('.contentWrapper').attr('class'))) {
            this.toggleCollapsedNavbar();
        }
        window.scrollTo(0, 0)
    }

    getNavbarLinks() {
        return (
            [
                ['/beers', 'beers'],
                ['/sodas', 'sodas'],
                ['/info/shipping', 'shipping'],
                ['/contact', 'contact']
            ].map((link, i) =>
                <LinkContainer to={link[0]} activeClassName='active' className='nav-link' key={i}
                               onClick={() => this.navbarLinkClickHandler()}>
                    <NavItem eventKey={i}><FormattedMessage {...messages[link[1]]} /></NavItem>
                </LinkContainer>
            )
        )
    }

    render() {
        var isAnonymous = isAuthenticated ? '' : ' is-anonymous';
        var isWide = localStorage.getItem('lang') == 'ru' ? ' wide' : '';

        return (
            <nav className='navbar navbar-default navbar-fixed-top'>
                <div className='container'>
                    <div className='navbar-head'>
                        <i className='fa fa-cog' onClick={() => this.toggleCollapsedNavbar()}/>

                        <div className='block-center'>
                            <LinkWrapper to={'/index'}>
                                <img src='/static/retro-beer-brand-one-line.png' alt='BEERCRAFT'/>
                            </LinkWrapper>
                        </div>

                        <CartWidget checkout={this.props.hideCartDropdown} collapsed/>

                    </div>

                    <div className={'navbars'+isAnonymous+isWide}>
                        <ul className='nav navbar-left'>
                            <li className='navbar-brand'>
                                <LinkWrapper to={'/index'}>
                                    <img src='/static/retro-beer-brand.png' alt='BEERCRAFT' className='navbar-brand'/>
                                </LinkWrapper>
                            </li>
                        </ul>


                        <ul ref='collapsible' className='nav navbar-nav collapsed is-contracted'>
                            {this.getNavbarLinks()}
                            <NavAccountLink setParentState={state => this.setState(state)}/>
                        </ul>


                        <ul className='nav navbar-right'>
                            <CartWidget checkout={this.props.hideCartDropdown}/>
                        </ul>

                    </div>
                </div>


                <LoginModal showModal={this.state.showLoginModal}
                            switchModal={modal => this.switchModals(modal)}
                            close={() => this.setState({showLoginModal: false})}/>
                <RegisterModal showModal={this.state.showRegisterModal}
                               switchModal={modal => this.switchModals(modal)}
                               close={() => this.setState({showRegisterModal: false})}/>
                <ResetModal showModal={this.state.showResetModal}
                            switchModal={modal => this.switchModals(modal)}
                            close={() => this.setState({showResetModal: false})}/>
            </nav>
        );
    }
}

class NavAccountLink extends React.Component {
    constructor() {
        super();
    }

    render() {
        if (!isAuthenticated) {
            var isWide = localStorage.getItem('lang') == 'ru' ? ' wide' : '';
            return (
                <li className={'nav-link nav-buttons'+isWide}>
                    <div className='navbar-btn'>
                        <a className='btn btn-default'
                           onClick={() => this.props.setParentState({showRegisterModal: true})}>
                            <FormattedMessage {...messages.signup} />
                        </a>


                        <a className='btn btn-default'
                           onClick={() => this.props.setParentState({showLoginModal: true})}>
                            <FormattedMessage {...messages.login} />
                        </a>
                    </div>
                </li>
            );
        } else {
            return (
                <li className='nav-link dropdown'>
                    <LinkWrapper to={'/account/settings'} className='dropdown-toggle'
                                 onMouseLeave={event => $(event.target).parent().removeClass('open')}
                                 onMouseEnter={event => $(event.target).parent().addClass('open')}>
                        <FormattedMessage {...messages.account} />
                    </LinkWrapper>
                    <ul className='dropdown-menu'
                        onMouseEnter={event => $(event.target).closest('li.dropdown').addClass('open')}
                        onMouseLeave={event => $(event.target).closest('li.dropdown').removeClass('open')}>
                        <li><LinkWrapper to={'/account/settings'}>
                            <FormattedMessage {...messages.settings} />
                        </LinkWrapper></li>
                        <li><LinkWrapper to={'/account/orders'}>
                            <FormattedMessage {...messages.history} />
                        </LinkWrapper></li>
                        <li><LinkWrapper to={'/account/discounts'}>
                            <FormattedMessage {...messages.discounts} />
                        </LinkWrapper></li>
                        <li><a href='/logout'>
                            <FormattedMessage {...messages.signout} />
                        </a></li>
                    </ul>
                </li>
            );
        }
    }
}