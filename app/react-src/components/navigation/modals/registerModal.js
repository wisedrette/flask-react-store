import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../shared_components/inputField.js'

var RegisterModal = React.createClass({

    getInitialState() {
        return {nameError: '', pwdError: '', emailError: ''}
    },

    serialize() {
        return JSON.stringify({
            username: this.refs.username.getValue(),
            email: this.refs.email.getValue(),
            password: this.refs.password.getValue()})
    },

    submitRegister(event) {
        var passed = true;
        Object.keys(this.refs).forEach( key => passed = this.refs[key].validate() && passed );

        if (passed) {
            $.ajax({
                method: 'POST',
                url: '/api/register',
                contentType: 'application/json',
                data: this.serialize(),
                success: function (data) {
                    if (data.register) {
                        location.replace('/index');
                    } else {
                        $('#registerForm').addClass('has-error').find('p.help-block').text('Username or email already in use');
                    }
                }
            });
        }
        $(event.target).blur();
    },

    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton />
                <Modal.Body>
                    <h4 className='title'>Register</h4>
                    <form method='post' id='registerForm'>
                        <p className='help-block'></p>

                        <div className='form-group'>
                            <InputField type='text' placeholder='Username'
                                        dataType='username' ref='username'/>
                            <InputField type='email' placeholder='Email'
                                        dataType='email' ref='email'/>
                            <InputField type='password' placeholder='Password'
                                        dataType='password' ref='password'/>
                        </div>
                        <Button bsStyle='success' id='submit_register' onClick={this.submitRegister} block>Sign up now</Button>
                    </form>
                    <small>By signing up you acknowledge that you are 21 or older.</small>
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block' onClick={() => this.props.switchModal('showLoginModal')}>
                            I already have an account
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
});

export default RegisterModal;