import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../shared_components/inputField.js'

export default class LoginModal extends React.Component{
    constructor(props){
        super(props);
        this.state = {emailError: false, pwdError: false}
    }

    serialize() {
        return JSON.stringify({email: this.refs.email.getValue(), password: this.refs.password.getValue()})
    }

    submitLogin(event) {
        var passed = true;
        Object.keys(this.refs).forEach( key => passed = this.refs[key].validate() && passed );

        if (passed) {
            $.ajax({
                method: 'POST',
                url: '/api/login',
                contentType: 'application/json',
                data: this.serialize(),
                success: function (data) {
                    if (data.login) {
                        this.props.close();
                        location.reload();
                    } else {
                        $('#loginForm > p.help-block').text('Invalid email or password');
                        $('#loginForm').addClass('has-error');
                    }
                }.bind(this)
            });
        }
        $(event.target).blur();
    }

    render() {
        return (
            <Modal show={this.props.showModal} onHide={() => this.props.close()} className='modalForm'>
                <Modal.Header closeButton />
                <Modal.Body>
                    <h4 className='title'>Login</h4>
                    <form method='post' id='loginForm'>
                        <p className='help-block'></p>

                        <div className='form-group'>
                            <InputField type='text' placeholder='Email' ref='email'/>
                            <InputField type='password' placeholder='Password' ref='password'/>
                        </div>
                        <Button bsStyle='success' id='submit_login' onClick={e => this.submitLogin(e)} block>Log In now</Button>
                    </form>
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showResetModal')}>
                            Reset password
                        </a>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showRegisterModal')}>
                            I'm new here
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}