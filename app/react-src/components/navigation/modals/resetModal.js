import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../shared_components/inputField.js'


export default class ResetModal extends React.Component {

    constructor() {
        super();
        this.state = {proceed: false, email: ''}
    }

    serialize() {
        return JSON.stringify({username: this.refs.username.getValue(), password: this.refs.password.getValue()})
    }

    switch(modal) {
        this.setState({proceed: false, email: ''});
        this.props.switchModal(modal);
    }

    render() {
        var formGroup;
        if (this.state.email == '') {
            formGroup = <EmailForm onClick={email => this.setState({email: email})}/>;
        } else {
            if (this.state.proceed)
                formGroup = <PasswordResetForm onClick={() => location.reload()} email={this.state.email}/>;
            else
                formGroup =
                    <CodeConfirmationForm onClick={() => this.setState({proceed: true})} email={this.state.email}/>;
        }

        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton/>
                <Modal.Body>
                    <h4 className='title'>ACCOUNT</h4>
                    {formGroup}
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.switch('showRegisterModal')}>
                            I'm new here
                        </a>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.switch('showLoginModal')}>
                            Log in
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}


class EmailForm extends React.Component {
    constructor() {
        super();
        this.state = {processing: false}
    }

    handleSubmit(e){
        e.preventDefault();
        this.handleClick();
    }

    handleClick() {
        if (this.refs.email.validate()) {
            $.ajax({
                url: '/api/pwd_reset/email',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({email: this.refs.email.getValue()}),
                beforeSend: (() => this.setState({processing: true})),
                success: (function (data) {
                    if (data)
                        this.props.onClick(this.refs.email.getValue());
                    else {
                        this.setState({processing: false});
                        this.refs.email.setValidationError('Such user doesn\'t exist')
                    }
                }.bind(this))
            });
        }
    }

    render() {
        var faClass = 'fa fa-refresh fa-3x fa-spin';
        return (
            <div>
                <i className={this.state.processing ? faClass : faClass + ' hidden'}/>
                <form method='post' id='loginForm'
                      className={this.state.processing ? 'hidden' : ''}
                      onSubmit={e => this.handleSubmit(e)}>
                    <p className='help-block'/>
                    <div className='form-group'>
                        <InputField type='text' placeholder='Email'
                                    ref='email' dataType='email'/>
                    </div>
                    <Button bsStyle='success' id='submit_reset' block onClick={() => this.handleClick()}>
                        Get new password
                    </Button>
                </form>
            </div>
        )
    }
}

class CodeConfirmationForm extends React.Component {
    constructor() {
        super();
        this.state = {processing: false}
    }

    handleSubmit(e){
        e.preventDefault();
        this.handleClick();
    }

    handleClick() {
        if (this.refs.code.validate()) {
            $.ajax({
                url: '/api/pwd_reset/code',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({code: this.refs.code.getValue(), email: this.props.email}),
                beforeSend: (() => this.setState({processing: true})),
                success: (function (data) {
                    if (data)
                        this.props.onClick();
                    else {
                        this.setState({processing: false});
                        this.refs.code.setValidationError('Confirmation code is invalid')
                    }
                }.bind(this))
            });
        }
    }

    render() {
        var faClass = 'fa fa-refresh fa-3x fa-spin';
        return (
            <div>
                <i className={this.state.processing ? faClass : faClass + ' hidden'}/>
                <form method='post' id='loginForm'
                      className={this.state.processing ? 'hidden' : ''}
                      onSubmit={e => this.handleSubmit(e)}>
                    <p className='help-block'/>
                    <div className='form-group'>
                        <InputField type='text' placeholder='Confirmation code from email' ref='code'/>
                    </div>
                    <Button bsStyle='success' id='submit_reset' block onClick={() => this.handleClick()}>
                        Confirm
                    </Button>
                </form>
            </div>
        )
    }
}

class PasswordResetForm extends React.Component {
    constructor() {
        super();
        this.state = {processing: false}
    }

    handleSubmit(e){
        e.preventDefault();
        this.handleClick();
    }

    handleClick() {
        var passed = true;
        Object.keys(this.refs).forEach(key => passed = this.refs[key].validate() && passed);
        if (this.refs.password.getValue() != this.refs.repeat.getValue()) {
            this.refs.repeat.setValidationError('Passwords don\'t match');
            passed = false;
        }

        if (passed) {
            $.ajax({
                url: '/api/pwd_reset/pwd',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({password: this.refs.password.getValue(), email: this.props.email}),
                beforeSend: (() => this.setState({processing: true})),
                success: (function (data) {
                    if (data)
                        this.props.onClick();
                    else {
                        this.setState({processing: false});
                        this.refs.password.setValidationError('Something went wrong :(');
                    }
                }.bind(this))
            });
        }
    }

    render() {
        var faClass = 'fa fa-refresh fa-3x fa-spin';
        return (
            <div>
                <i className={this.state.processing ? faClass : faClass + ' hidden'}/>
                <form method='post' id='loginForm'
                      className={this.state.processing ? 'hidden' : ''}
                      onSubmit={e => this.handleSubmit(e)}>
                    <p className='help-block'/>
                    <div className='form-group'>
                        <InputField type='password' placeholder='New password'
                                    ref='password' dataType='password'/>
                        <InputField type='password' placeholder='Repeat new password'
                                    ref='repeat' dataType='password'/>
                    </div>
                    <Button bsStyle='success' id='submit_reset' block onClick={() => this.handleClick()}>
                        Save password
                    </Button>
                </form>
            </div>
        )
    }
}
