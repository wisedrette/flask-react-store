import React from 'react'
import LinkWrapper from '../shared_components/linkWrapper.js'
import { connect } from 'react-redux'
import { update } from 'react-intl-redux'
import { messages, ruMessages } from '../../helper_functions/intlMessages.js'
import { FormattedMessage } from 'react-intl';

var sitemapLinks = {
    beers: [['sale', '/beers?sort=price_up&page=1'], ['bestsellers', '/beers?sort=bestselling&page=1'], ['newest', '/beers?sort=date&page=1'], ['rated', '/beers?sort=rating&page=1']],
    sodas: [['sale', '/sodas?sort=price_up&page=1'], ['bestsellers', '/sodas?sort=bestselling&page=1'], ['newest', '/sodas?sort=date&page=1'], ['rated', '/sodas?sort=rating&page=1']],
    info: [['ship_info', '/info/shipping'], ['policy', '/info/policies'], ['terms', '/info/terms'], ['contact', '/contact']],
    account: [['history', '/account/orders'], ['settings', '/account/settings'], ['discounts', '/account/discounts']]
};

export default class Footer extends React.Component {
    getSitemaps() {
        var sitemaps = Object.keys(sitemapLinks).map(function (key) {
            var links = sitemapLinks[key].map((item, i) =>
                <li key={key+i}>
                    <LinkWrapper to={item[1]} className='sitemap-item'>
                        <FormattedMessage {...messages[item[0]]}/>
                    </LinkWrapper>
                </li>
            );
            return (
                <div className='footer-sitemap' key={key}>
                    <a className='sitemap-link'><FormattedMessage {...messages[key]}/></a>
                    <ul className='sitemap-list'>
                        {links}
                    </ul>
                </div>
            )
        });

        return (
            <div className='sitemaps container'>
                {sitemaps}
            </div>
        )
    }

    componentDidMount(){
        var lang = localStorage.getItem('lang') || 'en';
        if (lang == 'ru')
            this.props.dispatch(update({locale: lang, messages: ruMessages}));
    }

    render() {
        var lang = this.props.locale == 'en' ? 'ENGLISH' : 'РУССКИЙ';
        return (
            <div className='main-footer' role='navigation'>
                <div className='header-wrapper container'>
                    <div className='header-left'>
                        <ul>
                            <li>
                                <LinkWrapper to={'/account/discounts'} checkAuthorization>
                                    <FormattedMessage {...messages.redeem}/>
                                </LinkWrapper>
                            </li>
                            <li><LinkWrapper to={'/contact'}><FormattedMessage {...messages.contact}/></LinkWrapper></li>
                            <li><LinkWrapper to={'/contact'}><FormattedMessage {...messages.brewery_submit}/></LinkWrapper></li>
                        </ul>
                    </div>

                    <div className='header-right'>
                        <FooterDropdown name='lang' value={lang}
                                        selectLang={state => this.props.dispatch(update(state))}
                        />

                        <FooterDropdown name='curr' value='USD'/>
                    </div>
                </div>
                <hr />

                {this.getSitemaps()}

                <div className='dark-footer'>
                    <div className='container'>
                        <ul>
                            <li>Powered by Flask and React, inspired by gog.com :D</li>
                        </ul>
                    </div>
                </div>

            </div>
        );
    }
}

class FooterDropdown extends React.Component {
    selectLocale(lang){
        var messages = {};
        if (lang != 'en')
            messages = ruMessages;
        this.props.selectLang({locale: lang, messages: messages});
        localStorage.setItem('lang', lang);
    }

    render() {
        return (
            <div className={'dropdown header-switch ' + this.props.name}>
                <span className='dropdown-toggle sorting' id='sortMenu'
                      data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                    <span><FormattedMessage {...messages[this.props.name]}/>:&nbsp;</span> {this.props.value}
                    <i className='caret'/><i className='glyphicon glyphicon-triangle-top'/>
                </span>

                <ul className='dropdown-menu' aria-labelledby='langMenu'>
                    <li onClick={() => this.selectLocale('en')}><a>English</a></li>
                    <li onClick={() => this.selectLocale('ru')}><a>Русский</a></li>
                </ul>
            </div>
        )
    }
}

export default connect(
    // Connect React Intl with Redux to inject current locale data
    ({ intl }) => ({...intl, key: intl.locale})
)(Footer)
