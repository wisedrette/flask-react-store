import React from 'react'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { messages } from '../../../helper_functions/intlMessages.js'

export default class Accordion extends React.Component {
    selectSection(e){
        var el = $(e.target).closest('div.about-header').get(0);
                $(el).toggleClass('active');
        $(this.refs[el.id]).toggleClass('active');

        $('.about-header').not(el).removeClass('active');
        $('.about-desc').not(this.refs[el.id]).removeClass('active');
    }

    render() {
        return (
            <div className='about'>
                <div className='about-headers'>
                    <div className='about-header' id='a1' onClick={e => this.selectSection(e)} >
                        <i className='fa fa-3x fa-dropbox'/>
                        <p><FormattedHTMLMessage {...messages.free_shipping} /></p>
                    </div>
                    <div className='about-header' id='a2' onClick={e => this.selectSection(e)} >
                        <i className='fa fa-3x fa-tags'/>
                        <p><FormattedHTMLMessage {...messages.no_tax} /></p>
                    </div>
                    <div className='about-header' id='a3' onClick={e => this.selectSection(e)} >
                        <i className='fa fa-3x fa-umbrella'/>
                        <p><FormattedHTMLMessage {...messages.destination_check} /></p>
                    </div>
                </div>
                <div className='about-sections'>
                    <div className='about-desc' ref='a1'>
                        <FormattedHTMLMessage {...messages.free_shipping_text} />
                    </div>
                    <div className='about-desc' ref='a2'>
                        <FormattedHTMLMessage {...messages.no_tax_text} />
                    </div>
                    <div className='about-desc' ref='a3'>
                        <FormattedHTMLMessage {...messages.destination_check_text} />
                    </div>
                </div>
            </div>
        )
    }
}