import React from 'react'

export default class Carousel extends React.Component {
    render() {
        var images = this.props.imgList.map((item, i) =>
            <div className={i==0 ? 'item in-carousel active' : 'item in-carousel'} key={i+item}>
                <img src={'/static/img/carousel/'+item}/>
            </div>
        );
        return (
            <div id={this.props.id} className='carousel slide' data-ride='carousel'>
                <div className='carousel-inner' role='listbox'>
                    {images}
                </div>

                <a className='left carousel-control' href={'#' + this.props.id} role='button' data-slide='prev'>
                    <span className='glyphicon glyphicon-chevron-left' aria-hidden='true'/>
                    <span className='sr-only'>Previous</span>
                </a>
                <a className='right carousel-control' href={'#' + this.props.id} role='button' data-slide='next'>
                    <span className='glyphicon glyphicon-chevron-right' aria-hidden='true'/>
                    <span className='sr-only'>Next</span>
                </a>
            </div>
        )
    }
}