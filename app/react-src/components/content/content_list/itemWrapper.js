import React from 'react'

import { Button } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import Link from 'react-router'
import Image from '../../shared_components/image.js'
import LinkWrapper from '../../shared_components/linkWrapper'
import TextWrapper from './textWrapper.js'
import Stars from '../product_comp/stars.js'


export default class ItemWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.addBeerToCart = this.addBeerToCart.bind(this);
    }

    addBeerToCart(e) {
        var beer = {
            id: this.props.item.id,
            name: this.props.item.title,
            price: this.props.item.price,
            img_path: this.props.item.img_path,
            count: 1
        };
        e.preventDefault();
        e.stopPropagation();
        this.props.addToCart(beer);
    }

    truncate(isResized) {
        this.refs.text.truncate(isResized);
    }

    render() {
        var isNews = this.props.type == 'news';
        var date = isNews ? this.props.item.added : '';
        var rate = this.props.type == 'list' ? this.props.item.rate : undefined;
        var infoBuy = isNews ? '' :
            <InfoBuyPart available={this.props.item.available} price={this.props.item.price}
                         rate={rate} added={this.props.added} addBeerToCart={this.addBeerToCart}/>;

        var params = {name: this.props.item.path};

        return (
            <LinkWrapper to={this.props.item.path} params={params}>
                <div className={this.props.type+'-group-item-wrapper'}>
                    <div className={this.props.type+'-group-item'}>

                        <div className='img-holder'>
                            <Image src={this.props.item.img_path} wide={isNews}/>
                        </div>
                        <div className='info-holder'>
                            <TextWrapper title={this.props.item.title} text={this.props.item.description}
                                         type={this.props.type} date={date} ref="text"/>
                            {infoBuy}
                        </div>

                    </div>
                </div>
            </LinkWrapper>
        )
    }
}


class InfoBuyPart extends React.Component {
    render() {
        var availability = this.props.available ? 'In stock' : 'Not available';
        var availabilityClass = this.props.available ? '' : ' has-error';
        var buttonClass = this.props.available ? '' : 'disabled';

        var stars = this.props.rate != undefined ? <Stars rate={this.props.rate}/> : '';

        return (
            <div className="bottom-product-info">
                {stars}
                <div className='price'>
                    <Button onClick={this.props.available ? this.props.addBeerToCart : ''} className={buttonClass}>
                        {this.props.added ?
                            <i className='fa fa-shopping-cart fa-flip-horizontal'/> :
                            <FormattedNumber value={this.props.price} style="currency" currency="USD"/>
                        }
                    </Button>
                </div>

                <span className={"availability"+availabilityClass}>
                    {availability}
                </span>

                <div style={{clear: 'both'}}></div>
            </div>
        )
    }
}