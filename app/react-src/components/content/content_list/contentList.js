import React from 'react';
import { ListGroup } from 'react-bootstrap'
import ItemWrapper from './itemWrapper.js'

const ContentList = React.createClass({

    handleResize(){
        Object.keys(this.refs).forEach(ref => this.refs[ref].truncate(true));
    },

    componentDidMount: function () {
        window.addEventListener('resize', this.handleResize);
    },

    componentWillUnmount: function () {
        window.removeEventListener('resize', this.handleResize);
    },


    render: function () {
        var dataList = this.props.data.map(function (item, i) {
            if (this.props.view != 'news') {
                var added = this.props.added.indexOf(item.id) != -1;
                return (
                    <ItemWrapper item={item} key={i} addToCart={this.props.addToCart}
                                 added={added} ref={"item"+i} type={this.props.view}
                    />
                );
            } else
                return (
                    <ItemWrapper item={item} key={i} ref={"item"+i} type={this.props.view}
                    />
                )
        }.bind(this));

        return (<ListGroup className={this.props.view}>{dataList}</ListGroup>)
    }
});

export default ContentList;