import React from 'react'

import jQuery from 'jquery';
import trunk from '../../../trunk8/trunk8'


$(document).on('click', '.read-less', function (event) {
    $(this).parent().trunk8({
        lines: 2,
        fill: '&hellip;&nbsp;<a class="read-more">&raquo;&raquo;</a>'
    });

    event.preventDefault();
});

$(document).on('click', '.read-more', function (event) {
    $(this).parent().trunk8('revert').append('&nbsp;<a class="read-less">&laquo;&laquo;</a>');

    event.preventDefault();
});


export default class TextWrapper extends React.Component {

    truncate(isResized) {
        var selector = $(this.refs.truncate);
        if (isResized) {
            var text = /grid|list-sm/.test(this.props.type) ? this.props.title : this.props.text;
            selector.trunk8('update', text);
        } else if (this.props.type == 'list') {
            selector.trunk8({
                lines: 2,
                fill: '&hellip;&nbsp;<a class="read-more">&raquo;&raquo;</a>'
            });
        } else if (this.props.type == 'news') {
            var lines = $(this.refs.title).height() < 25 ? 3 : 2;
            selector.trunk8({
                lines: lines
            });
        } else
            selector.trunk8();

    }

    componentDidMount() {
        this.truncate();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.type != this.props.type) { // && nextProps.type == 'list') {
            $(this.refs.truncate).trunk8('revert');
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.title != this.props.title) {
            this.truncate(true);
        }
        if (prevProps.type != this.props.type) {
            this.truncate(false);
        }
    }

    render() {
        if (this.props.type == 'list') {
            return (
                <div className='text'>
                    <h4 className='title'>{this.props.title}</h4>
                    <div className='description' ref='truncate'>
                        {this.props.text}
                    </div>
                </div>
            );
        } else if (this.props.type == 'news') {
            return (
                <div className='text'>
                    <h4 className='title' ref='title'>{this.props.title}</h4>
                    <small>{this.props.date.replace('T', ' ')}</small>
                    <div className='description' ref='truncate'
                         onMouseOver={e => e.target.title=''}
                         onMouseOut={e => e.target.title=this.props.text}>
                        {this.props.text}
                    </div>
                </div>
            );
        } else {
            return (
                <div className='text'>
                    <h4 className='title ellipsis' ref='truncate'>{this.props.title}</h4>
                </div>
            );
        }
    }
}
