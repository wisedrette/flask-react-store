import React from 'react'
import { FormattedMessage} from 'react-intl';
import { messages } from '../../../../helper_functions/intlMessages.js'

export default class OrderSelector extends React.Component {
    render() {
        var sort, i, isShifted = '';
        if (localStorage.getItem('lang') == 'ru')
            isShifted = ' shifted';

        var title = this.props.currentOrder;
        if (/price/.test(title)){
            title = title.replace(/_(.)*/,'');
            var faClass="fa fa-arrow"+this.props.currentOrder.replace('price_','-');
            i = <i className={faClass}/>
        }
        sort = <span className='sortType'><FormattedMessage {...messages[title]}/>{i}</span>;
        return (
            <div className='dropdown header-switch'>
                <span className='dropdown-toggle sorting' id='sortMenu'
                      data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                    <FormattedMessage {...messages.order}/> {sort}
                    <i className='caret'/><i className='glyphicon glyphicon-triangle-top'/>
                </span>

                <ul className={'dropdown-menu' + isShifted} aria-labelledby='sortMenu'>
                    <li><a onClick={() => this.props.selectOrder('title')}><FormattedMessage {...messages.title}/></a></li>
                    <li>
                        <a onClick={() => this.props.selectOrder('price_up')}><FormattedMessage {...messages.price}/>
                            <i className="fa fa-arrow-up"/>
                        </a>
                    </li>
                    <li>
                        <a onClick={() => this.props.selectOrder('price_down')}><FormattedMessage {...messages.price}/>
                            <i className="fa fa-arrow-down"/>
                        </a>
                    </li>
                    <li><a onClick={() => this.props.selectOrder('bestselling')}><FormattedMessage {...messages.bestselling}/></a></li>
                    <li><a onClick={() => this.props.selectOrder('rating')}><FormattedMessage {...messages.rating}/></a></li>

                    <li><a onClick={() => this.props.selectOrder('date')}><FormattedMessage {...messages.date}/></a></li>
                </ul>
            </div>
        )
    }
}
