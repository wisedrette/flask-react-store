import React from 'react';
import { Link } from 'react-router'
import TypeLink from './typeLink'
import TypeCategory from './typeCategory'

class TypesList extends React.Component {
    getTypesForCategory(name, types, isRest) {
        return (
            <TypeCategory types={types} name={name} rest={isRest} key={name}
                          query={this.props.query} location={this.props.location}
                          selectType={this.props.selectType}
                          updateTypes={this.props.updateTypes} />
        )
    }

    getCategories() {
        return this.props.categories.map(category =>
            this.getTypesForCategory(
                category.name,
                this.props.types.filter(type => type.subcategory == category.id)
            )
        );
    }

    getAllTypes() {
        var categories = this.getCategories();
        var category_ids = this.props.categories.map(category => category.id);

        var unCategorized = this.props.types.filter(type => category_ids.indexOf(type.subcategory) == -1);

        var rest = [];
        if (unCategorized.length > 6)
            rest = unCategorized.splice(5, unCategorized.length - 5);

        unCategorized.forEach(type => categories.push(
            <TypeLink key={type.id} id={type.id} name={type.name} nested={false}
                      query={this.props.query} location={this.props.location}
                      selectType={this.props.selectType}/>)
        );

        if (rest.length != 0) {
            categories.push(this.getTypesForCategory('Other', rest, true))
        }

        return categories
    }

    render() {
        var typesList = this.getAllTypes();
        var listFloat = typesList.length > 4 ? 'pull-right' : 'pull-left';

        return (
            <ul className={'nav navbar-nav ' + listFloat}>
                {this.getAllTypes()}
            </ul>
        )
    }
}

export default TypesList