import React from 'react'
import { NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

export default class TypeLink extends React.Component {
    constructor(props) {
        super(props);
        this.selectType = this.selectType.bind(this);

        if (props.query.types != undefined) {
            var types = JSON.parse("[" + props.query.types + "]");
            this.state = {selected: types.indexOf(props.id) != -1}
        } else
            this.state = {selected: false}
    }

    isSelected() {
        return this.state.selected;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.query.types != this.props.query.types) {
            if (nextProps.query.types != undefined) {
                var types = JSON.parse("[" + nextProps.query.types + "]");
                this.setState({selected: types.indexOf(this.props.id) != -1});
            } else
                this.setState({selected: false});
        }
    }

    selectType(event) {
        $(event.target).closest('a').blur();
        this.props.selectType(parseInt(this.props.id));
        if (this.props.nested)
            this.setState({selected: !this.state.selected});
    }

    render() {
        var types = [], classes = 'filters';
        if (this.props.query.types != undefined)
            types = JSON.parse("[" + this.props.query.types + "]");

        if (types.indexOf(this.props.id) != -1)
            classes += ' active';

        var checkBox;
        if (this.props.nested)
            checkBox = <input type="checkbox" checked={this.state.selected} readOnly/>;

        return (
            <NavItem className={classes} key={this.props.id} onClick={this.selectType}>
                {checkBox}{this.props.name}
            </NavItem>
        )
    }
}