import React from 'react'
import TypeLink from './typeLink'

export default class TypeCategory extends React.Component {
    constructor() {
        super();
        this.clearCategoryTypes = this.clearCategoryTypes.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);
    }

    ifAnyTypeSelected() {
        var result = false;

        Object.keys(this.refs).forEach(key => result = result || this.refs[key].isSelected());
        return result;
    }

    getTypes() {
        return this.props.types.map(type =>
            <TypeLink key={type.id} id={type.id} name={type.name} nested={true}
                      query={this.props.query} location={this.props.location}
                      selectType={this.props.selectType} ref={type.id}/>
        );
    }

    clearCategoryTypes(event) {
        var types = JSON.parse("[" + this.props.query.types + "]");
        Object.keys(this.refs).forEach(function (key) {
            if (this.refs[key].isSelected()) {
                //this.props.selectType(parseInt(key));
                types.splice(types.indexOf(parseInt(key)), 1);
            }
        }.bind(this));

        this.props.updateTypes(types);
        event.stopPropagation();
        event.preventDefault();
    }

    toggleDropdown(event) {
        $(event.target).closest('li.filters').toggleClass('open');
        event.preventDefault();
    }

    render() {
        var categoryClassName = 'dropdown filters';
        var icon;
        if (this.props.rest)
            categoryClassName += ' rest';
        if (this.ifAnyTypeSelected()) {
            categoryClassName += ' active';
            icon = <i className='fa fa-remove' onClick={this.clearCategoryTypes}/>;
        } else
            icon = <i className='caret'/>;

        return (
            <li className={categoryClassName}>
                <a href='' className='extras' onClick={this.toggleDropdown}>{this.props.name}
                    {icon}<i className='glyphicon glyphicon-triangle-top'/>
                </a>
                <ul className='dropdown-menu'>
                    {this.getTypes()}
                </ul>
            </li>
        )
    }
}