import React from 'react';
import delay from '../../../../helper_functions/delay.js'


export default class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: props.query};
    }

    handleInputStateChange(event) {
        this.setState({value: event.target.value});
    }

    clearInput() {
        this.clearInputState();
        this.props.searchQuery('');
    }

    clearInputState() {
        this.setState({value: ''});
    }

    render() {
        return (
            <div className='search-form'>
                <form className='navbar-form' role='search'>
                    <div className='input-group'>
                        <div className='input-group-btn'>
                                    <span className='btn btn-default search-btn'>
                                        <i className='glyphicon glyphicon-search'/>
                                    </span>
                        </div>
                        <input type='text' className='form-control' placeholder='Search'
                               value={this.state.value}
                               onChange={e => this.handleInputStateChange(e)}
                               onKeyUp={() => this.props.searchQuery(this.state.value) }/>

                        <div className='input-group-btn'>
                                    <span className='btn btn-default search-btn' onClick={() => this.clearInput()}>
                                        <i className='glyphicon glyphicon-remove'/>
                                    </span>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}