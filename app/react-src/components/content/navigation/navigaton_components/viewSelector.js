import React from 'react'
import { FormattedMessage} from 'react-intl';
import { messages } from '../../../../helper_functions/intlMessages.js'

export default class ViewSelector extends React.Component {
    render() {
        return (
            <div className='dropdown header-switch'>
                <span className='dropdown-toggle sorting' id='viewMenu'
                      data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                    <FormattedMessage {...messages.view} />
                    <span className='sortType'><FormattedMessage {...messages[this.props.currentView]} /></span>
                    <i className='caret'/><i className='glyphicon glyphicon-triangle-top'/>
                </span>

                <ul className='dropdown-menu' aria-labelledby='viewMenu'>
                    <li><a onClick={() => this.props.selectView('grid')}><FormattedMessage {...messages.grid} /></a></li>
                    <li><a onClick={() => this.props.selectView('list')}><FormattedMessage {...messages.list} /></a></li>
                </ul>
            </div>
        )
    }
}