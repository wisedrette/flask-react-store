import React from 'react';
import { Pagination, Pager, PageItem } from 'react-bootstrap';
import { browserHistory } from 'react-router'
import delay from '../../../helper_functions/delay.js'


const ContentPager = React.createClass({
    getInitialState() {
        var currentPage = this.props.pageId;
        return {
            activePage: currentPage,
            value: currentPage,
            hasPrev: currentPage != 1,
            hasNext: currentPage != this.props.pages,
            timer: null
        };
    },

    changePage(value){
        if (/^\d+$/.test(value)) {
            this.props.selectPage(value);
            window.scrollTo(0,0);
        } else
            this.setState({value: this.state.activePage});
    },

    handleSelect(event) {
        this.setState({value: event.target.value});
    },

    handlePrevSelect() {
        this.setState({
            activePage: this.state.activePage - 1
        });
        this.changePage(this.state.activePage - 1);
    },

    handleFirstSelect() {
        this.setState({
            activePage: 1
        });
        this.changePage(1);
    },

    handleNextSelect() {
        this.setState({
            activePage: this.state.activePage + 1
        });
        this.changePage(this.state.activePage + 1);
    },

    handleLastSelect() {
        this.setState({
            activePage: this.props.pages
        });
        this.changePage(this.props.pages);
    },

    componentDidUpdate(prevProps){
        var currentPage;
        var pages;
        if (prevProps.pageId != this.props.pageId) {
            currentPage = this.props.pageId == undefined ? 1 : parseInt(this.props.pageId);
            pages = this.props.pages;
            this.setState({
                activePage: currentPage,
                value: currentPage,
                hasPrev: currentPage != 1,
                hasNext: currentPage != pages
            });
        }
        if (prevProps.pages != this.props.pages) {
            currentPage = this.props.pageId == undefined ? 1 : parseInt(this.props.pageId);
            pages = this.props.pages;
            this.setState({
                hasNext: currentPage != pages
            })
        }
    },

    render() {
        return (
            <div className='paging'>
                <span className={this.state.hasPrev ? '':'disabled'} onClick={() => this.handleFirstSelect()}>
                    <i className='fa fa-angle-double-left'/>
                </span>
                <span className={this.state.hasPrev ? '':'disabled'} onClick={() => this.handlePrevSelect()}>
                    <i className='fa fa-angle-left'/>
                </span>

                <div>
                    <input value={this.state.value} size='2' onChange={this.handleSelect}
                           onKeyUp={e => this.changePage(e.target.value)} onBlur={e => this.changePage(e.target.value)}
                    />&nbsp;&nbsp;of&nbsp;&nbsp;{this.props.pages}
                </div>

                <span className={this.state.hasNext ? '':'disabled'} onClick={() => this.handleNextSelect()}>
                    <i className='fa fa-angle-right'/>
                </span>
                <span className={this.state.hasNext ? '':'disabled'} onClick={() => this.handleLastSelect()}>
                    <i className='fa fa-angle-double-right'/>
                </span>
            </div>
        )
    }
});


export default ContentPager;