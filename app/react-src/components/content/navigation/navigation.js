import React from 'react';
import { ListGroup, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { Link } from 'react-router'
import { SearchBar, TypesList, ViewSelector, OrderSelector } from './navigaton_components/'

import { connect } from 'react-redux'
import { selectType, selectPage, selectView, selectOrder, searchQuery, clearFilter, updateTypes } from '../../../redux/actions/actions.js'

import { FormattedNumber, FormattedMessage, FormattedPlural } from 'react-intl';
import { RuFormattedPlural } from '../../shared_components'
import { messages } from '../../../helper_functions/intlMessages.js'


class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {types: [], count: 0, categories: [], collapsed: true};
    }

    ajaxRequestTypesList() {
        $.ajax({
            url: '/api/navigation' + this.props.location,
            dataType: 'json',
            cache: true,
            success: (data => this.setState(data))
        });
    }

    componentWillMount() {
        this.ajaxRequestTypesList();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location != this.props.location) {
            this.ajaxRequestTypesList();
            this.props.dispatch(clearFilter());
        }
    }

    clearFilters() {
        this.refs.searchBar.clearInputState();
        this.dispatch(clearFilter());
    }

    dispatch(action) {
        this.props.dispatch(action);
        this.props.updateParent();
    }

    render() {
        var counter;
        if (localStorage.getItem('lang') == 'ru'){
            if (/beer/.test(this.props.location))
                counter = <RuFormattedPlural value={this.state.count}
                                             one="живое пиво" few="живого пива"
                                             many="живого пива"/>;
            else
                counter = <RuFormattedPlural value={this.state.count}
                                             one="газировка" few="газировки"
                                             many="газировок"/>;
        } else {
            var text = this.props.location.replace('/', '');
            text = ' crafted ' + text.substring(0, text.length - 1);
            counter = <FormattedPlural value={this.state.count}
                                       one={text}
                                       other={text+'s'}/>;
        }
        return (
            <section className='navigation'>
                <section className='header-switches'>

                    <div className='left-part'>
                        <strong className="upper">{this.state.count} {counter}</strong>

                        <LinkContainer to={this.props.location} className='clear'>
                        <span onClick={() => this.clearFilters()}>
                            <i className='glyphicon glyphicon-remove'/>
                            <FormattedMessage {...messages.clear_filters} />
                        </span>
                        </LinkContainer>
                    </div>

                    <div className='right-part'>

                        <OrderSelector currentOrder={this.props.filter.sort}
                                       selectOrder={sort => this.dispatch(selectOrder(sort))}/>

                        <ViewSelector currentView={this.props.filter.view}
                                      selectView={ viewType => this.dispatch(selectView(viewType)) }/>
                    </div>

                    <div style={{clear: 'both'}}></div>

                </section>


                <div className='navbar navbar-default filters' role='navigation'>

                    <SearchBar searchQuery={query => this.dispatch(searchQuery(query)) } query={this.props.filter.search}
                               ref="searchBar"/>

                    <div className={this.state.collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse in'}>

                        <TypesList types={this.state.types} categories={this.state.categories}
                                   location={this.props.location} query={this.props.query}
                                   selectType={ id => this.dispatch(selectType(id)) }
                                   updateTypes={ types => this.dispatch(updateTypes(types)) }
                        />
                    </div>

                    <ul className='nav navbar-nav pull-right collapse-toggle'>
                        <li className='filters collapsed'>
                            <a onClick={() => this.setState({collapsed: !this.state.collapsed})}>Filters
                                <b className='caret'/></a>
                        </li>
                    </ul>
                </div>
            </section>
        )
    }
}

function select(state) {
    return {
        filter: state.filter.filter
    }
}

export default connect(select)(Navigation);