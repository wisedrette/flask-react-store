import React from 'react'
import marked from 'marked'
import Image from '../shared_components/image.js'
import LinkWrapper from '../shared_components/linkWrapper'
import TextWrapper from './content_list/textWrapper.js'
import { FormattedNumber, FormattedRelative, FormattedDate, FormattedMessage } from 'react-intl';
import { messages } from '../../helper_functions/intlMessages.js'
import { connect } from 'react-redux'

export default class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            news: {
                img_path: '',
                title: '',
                description: '',
                added: '',
                path: ''
            },
            prev: {
                title: '',
                path: ''
            },
            next: {
                title: '',
                path: ''
            },
            headlines: []
        }
    }

    ajaxRequestContent() {
        $.ajax({
            url: '/api/news/' + this.props.params.name,
            method: 'GET',
            dataType: 'json',
            cache: true,
            contentType: 'application/json',
            success: (data => this.setState(data))
        });
    }


    componentDidMount() {
        this.ajaxRequestContent();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.params.name != this.props.params.name) {
            this.ajaxRequestContent();
        }
    }

    rawMarkup() {
        var rawMarkup = marked(this.state.news.description.toString(), {sanitize: false});
        return {__html: rawMarkup};
    }

    render() {
        var headlines = this.state.headlines.map((headline, i) =>
            <div className='headline' key={i}>
                <LinkWrapper to={headline.path}>
                    <TextWrapper title={headline.title}/>
                    <small><FormattedHeadlineDate added={headline.added} locale={this.props.locale}/></small>
                </LinkWrapper>
            </div>
        );

        var prev, next;
        if (this.state.prev != null)
            prev = <LinkWrapper to={this.state.prev.path}>
                <i className='fa fa-3x fa-angle-left'/>
                <span>{this.state.prev.title}</span>
            </LinkWrapper>;
        if (this.state.next != null)
            next = <LinkWrapper to={this.state.next.path}>
                <span>{this.state.next.title}</span>
                <i className='fa fa-3x fa-angle-right'/>
            </LinkWrapper>;

        return (
            <div className='content product-details'>
                <div>
                    <h2 className='upper'>{this.state.news.title}</h2>
                    <strong><FormattedHeadlineDate added={this.state.news.added} locale={this.props.locale}/></strong>
                </div>
                <div className='column-left'>
                    <div className='media'>
                        <h4 className='upper'>
                            <FormattedMessage {...messages.media}/>
                        </h4>
                        <Image src={this.state.news.img_path} wide/>
                    </div>
                    <div className='info'>
                        <header className='description-header'>
                            <FormattedMessage {...messages.description}/>
                        </header>
                        <div className='description-text'>
                            <span dangerouslySetInnerHTML={this.rawMarkup()}/>
                        </div>
                    </div>

                    <div className='navigation'>
                        <div className='prev'>
                            {prev}
                        </div>
                        <div className='next'>
                            {next}
                        </div>
                    </div>
                </div>
                <div className='column-right'>
                    <h4 className='upper'>
                        <FormattedMessage {...messages.headlines}/>
                    </h4>
                    <div>
                        {headlines}
                    </div>
                </div>
            </div>
        )
    }
}


class FormattedHeadlineDate extends React.Component {
    render() {
        var date = <strong />;
        if (this.props.added != '') {
            var added = Date.parse(this.props.added);
            var lastMonth = Date.now() - (1000 * 60 * 60 * 24 * 30);
            if (lastMonth > added || this.props.locale != 'en')
                date = <FormattedDate value={added} day="numeric" month="long" year="numeric"/>;
            else
                date = <FormattedRelative value={added}/>;
        }
        return date;
    }
}

export default connect(
    // Connect React Intl with Redux to inject current locale data
    ({ intl }) => ({...intl, key: intl.locale})
)(News)