export { default as Content } from './content.js'
export { default as IndexPage } from './indexPage.js'
export { default as Product } from './product.js'
export { default as News } from './news.js'
