import React from 'react'

import marked from 'marked'
import Image from '../shared_components/image.js'
import { Stars, BuyPanel, ReviewModal, Recommendations } from './product_comp/'
import { connect } from 'react-redux'
import { addBeer } from '../../redux/actions/actions.js'
import { FormattedMessage, FormattedPlural } from 'react-intl';
import { RuFormattedPlural } from '../shared_components'
import { messages } from '../../helper_functions/intlMessages.js'

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            product: {
                img_path: '',
                title: '',
                description: '',
                types_id: [],
                rate: 0,
                sold: 0
            },
            types: [], reviews: [], votes: 0,
            showReviewModal: false
        };
    }

    ajaxRequestContent() {
        $.ajax({
            url: '/api/product/' + this.props.params.name,
            method: 'GET',
            dataType: 'json',
            cache: true,
            contentType: 'application/json',
            success: (data => this.setState(data))
        });
    }

    vote(rate) {
        $.ajax({
            url: '/api/rate/' + this.state.product.id,
            method: 'POST',
            dataType: 'json',
            cache: true,
            data: JSON.stringify({rate: rate}),
            contentType: 'application/json',
            success: (data => this.setState({vote: rate}))
        });
    }

    componentDidMount() {
        this.ajaxRequestContent();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.params.name != this.props.params.name) {
            this.ajaxRequestContent();
            $('.gimme-more').removeClass('hidden');
        }
    }

    rawMarkup() {
        var rawMarkup = marked(this.state.product.description.toString(), {sanitize: false});
        return {__html: rawMarkup};
    }

    render() {
        var reviews, counter;
        if (this.state.reviews.length == 0)
            reviews = (
                <div className='no-reviews text-center'>
                    <span><FormattedMessage {...messages.no_reviews} /></span>
                </div>
            );
        else {
            reviews = this.state.reviews.map(function(review, i){
                var titleAddition;
                if (review.user != undefined){
                    titleAddition = <span>&nbsp;&nbsp;by&nbsp;{review.user}</span>
                }
                return (
                    <div className='review' key={i}>
                        <p><strong>{review.title}</strong>{titleAddition}</p>
                        <p>{review.text}</p>
                        <small>{review.added_at.replace('T', ' ')}</small>
                    </div>
                )
            })
        }

        if (localStorage.getItem('lang') == 'ru')
            counter = (
                    <RuFormattedPlural value={this.state.votes}
                                       one="голос" few="голоса"
                                       many="голосов" other="голосов"/>
            );
        else
            counter = (
                    <FormattedPlural value={this.state.votes}
                                     one="vote"
                                     other="votes"/>
            );
        return (
            <div className='content product-details'>
                <h2 className='upper'>{this.state.product.title}</h2>
                <div className='rating'>
                    <Stars rate={this.state.product.rate}/>
                    <span className='votes'>{this.state.votes}&nbsp;{counter}</span>
                    <span className='types'>{this.state.types.join(', ')}</span>
                </div>
                <div className='column-left'>
                    <div className='media'>
                        <h4 className='upper'><FormattedMessage {...messages.media} /></h4>
                        <Image src={this.state.product.img_path}/>
                    </div>
                    <div className='info'>
                        <header className='description-header'><FormattedMessage {...messages.description} /></header>
                        <div className='description-text'>
                            <span dangerouslySetInnerHTML={this.rawMarkup()}/>
                        </div>

                        <section className='reviews'>
                            <header className='review-header'><FormattedMessage {...messages.reviews} /></header>
                            <div className='rating review-subheader'>
                                <span>
                                    <strong><FormattedMessage {...messages.av_rating} />:&nbsp;&nbsp;</strong>
                                    <Stars rate={this.state.product.rate}/>
                                </span>
                                <div className='pull-right'>
                                    <strong><FormattedMessage {...messages.my_rating} />:&nbsp;</strong>
                                    <Stars rate={this.state.vote} vote={rate => this.vote(rate)}/>
                                    <strong><FormattedMessage {...messages.my_review} />:&nbsp;
                                        <span className='add-review'
                                              onClick={() => this.setState({showReviewModal: true})}>
                                            <FormattedMessage {...messages.add_review} />
                                        </span>
                                    </strong>
                                </div>
                            </div>
                            {reviews}
                        </section>
                    </div>
                </div>
                <div className='column-right'>
                    <h4 className='upper'><FormattedMessage {...messages.buy} /></h4>
                    <BuyPanel item={this.state.product} added={this.props.beers.indexOf(this.state.product.id) != -1}
                              addToCart={ beer => this.props.dispatch(addBeer(beer)) }/>
                    <Recommendations id={this.state.product.id} beers={this.props.beers}
                                     addToCart={ beer => this.props.dispatch(addBeer(beer))}/>
                </div>

                <ReviewModal id={this.state.product.id}
                             vote={rate => this.vote(rate)}
                             title={this.state.product.title}
                             showModal={this.state.showReviewModal}
                             close={() => this.setState({showReviewModal: false})}/>
            </div>
        )
    }
}

function select(state) {
    return {
        beers: state.beerList.beers.map(beer => beer.id)
    }
}

export default connect(select)(Product)
