import React from 'react'
import { Navigation, ContentPager } from './navigation/'
import ContentList from './content_list/contentList.js'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { addBeer, selectPage, updateFilter } from '../../redux/actions/actions.js'


class Content extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: [], filterChanged: false};
    }

    ajaxRequestContent(pathname) {
        $.ajax({
            url: '/api/content/' + pathname.replace('/', ''),
            method: 'POST',
            dataType: 'json',
            cache: false,
            contentType: 'application/json',
            data: JSON.stringify(this.props.filter),
            success: (data => this.setState(data))
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.pathname != this.props.location.pathname) {
            this.ajaxRequestContent(nextProps.location.pathname);
        }
    }

    componentDidUpdate(prevProps) {
        if (this.state.filterChanged) {
            this.setState({filterChanged: false});
            this.ajaxRequestContent(this.props.location.pathname);
        }

        else if (prevProps.location.query != this.props.location.query) {
            this.props.dispatch(updateFilter());
            this.setState({filterChanged: true});
        }
    }

    componentWillMount() {
        this.ajaxRequestContent(this.props.location.pathname);
    }

    render() {
        const { beers } = this.props;
        return (
            <div className='content'>
                <div className='navigation'>
                    <Navigation location={this.props.location.pathname} query={this.props.location.query}
                                updateParent={ () => this.setState({filterChanged: true})}
                    />
                </div>
                <div>
                    <ContentList data={this.state.data} added={beers} view={this.props.filter.view}
                                 addToCart={ beer => this.props.dispatch(addBeer(beer)) }/>
                    <div style={{clear: 'both'}}></div>
                </div>
                <div>
                    <ContentPager pageId={this.props.filter.page} pages={this.state.pages}
                                  location={this.props.location.pathname} query={this.props.location.query}
                                  selectPage={ id => this.props.dispatch(selectPage(id)) }
                    />
                </div>
            </div>
        );
    }
}

function select(state) {
    return {
        beers: state.beerList.beers.map(beer => beer.id),
        filter: state.filter.filter
    }
}

export default connect(select)(Content)