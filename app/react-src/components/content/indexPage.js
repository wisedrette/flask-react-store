import React from 'react'
import ContentList from './content_list/contentList.js'
import LinkWrapper from '../shared_components/linkWrapper'

import { Carousel, Accordion } from './index_comp'
import { FormattedMessage } from 'react-intl';
import { messages } from '../../helper_functions/intlMessages.js'

import { connect } from 'react-redux'
import { addBeer, clearFilter, selectOrder } from '../../redux/actions/actions.js'

class IndexPage extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            data_bestselling: [],
            data_date: [],
            data_rating: [],
            news_list: [],
            news_number: 11,
            display: 'bestselling'
        };
    }

    ajaxRequestContent(type) {
        $.ajax({
            url: '/api/index_content/' + type,
            method: 'GET',
            dataType: 'json',
            cache: false,
            contentType: 'application/json',
            success: (function (data) {
                this.setState(data);
            }.bind(this))
        });
    }

    componentDidMount() {
        this.ajaxRequestContent('recent');
        this.ajaxRequestContent('news');
        this.ajaxRequestContent('bestsellers');
        this.ajaxRequestContent('new');
        this.ajaxRequestContent('rating');
    }

    updateFilters() {
        this.props.dispatch(clearFilter());
        this.props.dispatch(selectOrder(this.state.display));
    }

    increaseNewsNumber() {
        if (this.state.news_number + 5 < this.state.news_list.length)
            this.setState({news_number: this.state.news_number + 5});
        else
            this.setState({news_number: this.state.news_list.length});
    }

    switchState(state){
        this.setState({display: state});
    }

    render() {
        var horImages = ['1.jpg', '2.jpg', '3.jpg', '4.jpg'];
        var verImages = ['v1.jpg', 'v2.jpg'];
        var moreNews;
        if (this.state.news_number < this.state.news_list.length)
            moreNews =
                <div className='gimme-more' onClick={() => this.increaseNewsNumber()}>
                    <FormattedMessage {...messages.more_news} />
                </div>;
        return (
            <div className='index content'>
                <div className='carousels'>
                    <div className='carousel-wrapper'>
                        <h4><FormattedMessage {...messages.what_new} /></h4>
                        <Carousel imgList={horImages} id='h-carousel'/>
                    </div>
                    <div className='carousel-wrapper vertical'>
                        <h4><FormattedMessage {...messages.deals} /></h4>
                        <Carousel imgList={verImages} id='v-carousel'/>
                    </div>
                </div>
                <div className='index-products'>
                    <ContentList data={this.state.data} added={this.props.beers} view='grid'
                                 addToCart={ beer => this.props.dispatch(addBeer(beer)) }/>
                    <div style={{clear: 'both'}}></div>
                </div>
                <div className='products'>
                    <div className='breadcrumbs'>
                        <a onClick={() => this.switchState('bestselling')}
                           onTouchStart={() => this.switchState('bestselling')}
                           className={this.state.display == 'bestselling' ? 'active' : ''}>
                            <FormattedMessage {...messages.popular} />
                        </a>
                        <i className='fa fa-circle'/>
                        <a onClick={() => this.switchState('date')}
                           onTouchStart={() => this.switchState('date')}
                           className={this.state.display == 'date' ? 'active' : ''}>
                            <FormattedMessage {...messages.new} />
                        </a>
                        <i className='fa fa-circle'/>
                        <a onClick={() => this.switchState('rating')}
                           onTouchStart={() => this.switchState('rating')}
                           className={this.state.display == 'rating' ? 'active' : ''}>
                            <FormattedMessage {...messages.rated} />
                        </a>
                    </div>
                    <ContentList data={this.state['data_' + this.state.display]} added={this.props.beers} view='list-sm'
                                 addToCart={ beer => this.props.dispatch(addBeer(beer)) }/>

                    <div className='gimme-more'>
                        <LinkWrapper to={'/beers?sort='+this.state.display}>
                            <div onClick={() => this.updateFilters()}>
                                {(() => {
                                    switch (this.state.display) {
                                        case 'rating':
                                            return <FormattedMessage {...messages.more_rated} />;
                                        case 'date':
                                            return <FormattedMessage {...messages.more_new} />;
                                        default:
                                            return <FormattedMessage {...messages.more_popular} />;
                                    }
                                })()}

                            </div>
                        </LinkWrapper>
                    </div>
                </div>

                <div className='small-info'>
                    <h4><FormattedMessage {...messages.why_here} /></h4>
                    <Accordion />
                </div>

                <div className='headlines'>
                    <h4><FormattedMessage {...messages.headline} /></h4>
                    <ContentList data={this.state.news_list.slice(0, this.state.news_number)} view='news'/>
                    {moreNews}
                </div>
            </div>
        );
    }
}


function select(state) {
    return {
        beers: state.beerList.beers.map(beer => beer.id)
    }
}

export default connect(select)(IndexPage)