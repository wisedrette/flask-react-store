import React from 'react'
import { Button } from 'react-bootstrap'
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { messages } from '../../../helper_functions/intlMessages.js'

export default class BuyPanel extends React.Component {
    addToCart(e) {
        if (this.props.item.available) {
            var beer = {
                id: this.props.item.id,
                name: this.props.item.title,
                price: this.props.item.price,
                img_path: this.props.item.img_path,
                count: 1
            };
            this.props.addToCart(beer);
        } else {
            e.preventDefault();
            e.stopPropagation();
        }
    }

    render() {
        var buttonClass = this.props.item.available ? '' : ' disabled';
        var price = this.props.item.price == undefined ? 0 : this.props.item.price;
        return (
            <div className='buy-panel text-right'>
                <strong><FormattedNumber value={price} style="currency" currency="USD"/></strong>

                <Button onClick={e => this.addToCart(e)} className={'text-right'+buttonClass}>
                        <span><i className='fa fa-shopping-cart fa-flip-horizontal'/>&nbsp;
                            <FormattedMessage {...messages.add_to_cart}/>
                        </span>
                </Button>

            </div>
        )
    }
}
