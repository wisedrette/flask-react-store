import React from 'react'
import {  Modal, Button, Input } from 'react-bootstrap'
import Stars from './stars.js'
//import InputField from '../../shared_components/inputField.js'

export default class ReviewModal extends React.Component {
    constructor(props){
        super(props);
        this.state = {disabled: true, vote: undefined}
    }

    submitReview() {
        if (isAuthenticated && this.state.vote != undefined) {
            this.props.vote(this.state.vote)
        }

        var review = JSON.stringify({
            title: this.refs.title.getValue(),
            text: this.refs.text.getValue()
        });
        $.ajax({
            url: '/api/review/' + this.props.id,
            method: 'POST',
            dataType: 'json',
            cache: true,
            data: review,
            contentType: 'application/json',
            success: (() => this.props.close())
        });
    }

    handleChange(){
        if (this.refs.title.getValue().length != 0 && this.refs.text.getValue().length != 0)
            this.setState({disabled: false});
        else
            if (!this.state.disabled)
                this.setState({disabled: true});
    }

    render() {
        var before = <span>My rating:&nbsp;<Stars vote={rate => this.setState({vote: rate})}/></span>;
        return (
            <Modal show={this.props.showModal} onHide={() => this.props.close()} className='modalForm review'>
                <Modal.Header closeButton/>
                <Modal.Body>
                    <div className='headers'>
                        <header className='upper'>Add your review for</header>
                        <header className='upper'><strong>{this.props.title}</strong></header>
                    </div>
                    <form method='post' id='reviewForm'>
                        <div className='form-group'>
                            <Input type='text' placeholder='Review title' ref='title'
                                   addonBefore={before} onChange={() => this.handleChange()}/>
                            <Input type='textarea' placeholder='Review test' ref='text'
                                   onChange={() => this.handleChange()}/>
                        </div>
                        <Button onClick={() => this.submitReview()} className={this.state.disabled? 'disabled': ''}>
                            Add review
                        </Button>
                    </form>
                </Modal.Body>
            </Modal>
        );
    }
}
