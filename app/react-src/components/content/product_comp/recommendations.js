import React from 'react';
import ContentList from '../content_list/contentList.js'
import { FormattedMessage } from 'react-intl';
import { messages } from '../../../helper_functions/intlMessages.js'

class Recommendations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data_s: [],
            data_o: [],
            processing_s: false,
            processing_o: false
        };
    }

    ajaxRequestContent(type) {
        $.ajax({
            url: '/api/similar/' + type + this.props.id,
            method: 'GET',
            dataType: 'json',
            cache: true,
            contentType: 'application/json',
            success: (data => this.setState(data))
        });
    }

    componentDidMount() {
        if (this.props.id != undefined) {
            this.ajaxRequestContent('types/');
            this.ajaxRequestContent('orders/');
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.id != this.props.id) {
            this.ajaxRequestContent('types/');
            this.ajaxRequestContent('orders/');
        }
    }

    loadMore(key) {
        var page = this.state['data_' + key].length / 5 + 1;
        var link = '/api/similar/';
        if (key == 's')
            link += 'types/';
        else
            link += 'orders/';
        $.ajax({
            url: link + this.props.id + '/' + page,
            method: 'GET',
            dataType: 'json',
            cache: true,
            contentType: 'application/json',
            beforeSend: (function () {
                var state = {};
                state['processing_' + key] = true;
                this.setState(state);
            }.bind(this)),
            success: (function (data) {
                var sName= 'data_' + key;
                var dataArray = this.state[sName];
                data[sName].forEach(item => dataArray.push(item));
                var state = {};
                state[sName] = dataArray;
                state['processing_'+key] = false;
                this.setState(state);
                if (page >= data.pages) {
                    $(this.refs[key]).addClass('hidden');
                }
            }.bind(this))
        });
    }

    render() {
        var recommendations = ['s', 'o'].map(
            function (key) {
                var title = key == 's' ? 'also_like' : 'also_bought';
                if (this.state['data_' + key].length != 0)
                    return (
                        <div key={key}>
                            <h4 className='upper'><FormattedMessage {...messages[title]} /></h4>
                            <ContentList data={this.state['data_'+key]} view='list-sm' added={this.props.beers}
                                         addToCart={ beer => this.props.addToCart(beer) }/>

                            <div className='gimme-more' ref={key}>
                                <i className={this.state['processing_'+key] ? 'fa fa-refresh fa-2x fa-spin' : 'hidden'}/>
                                <div onClick={() => this.loadMore(key)}
                                     className={this.state['processing_'+key] ? 'hidden' : ''}>
                                    <FormattedMessage {...messages.more_recommendations} />
                                </div>
                            </div>
                        </div>
                    );
            }.bind(this)
        );
        return (
            <div className='recommendations'>
                {recommendations}
            </div>
        )
    }
}


export default Recommendations