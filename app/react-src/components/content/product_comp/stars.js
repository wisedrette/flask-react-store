import React from 'react'
import ReactTestUtils from 'react-addons-test-utils'


export default class Stars extends React.Component {
    constructor() {
        super();
        this.state = {prev: 0};
        this.handleSingleHover = this.handleSingleHover.bind(this)
    }

    handleHover(e) {
        $(e.target).find('.fa').removeClass('fa-circle').addClass('fa-star');
    }

    handleLeave(e) {
        $(e.target).find('.fa').addClass('fa-circle').removeClass('fa-star is-full');
    }

    handleSingleHover(e) {
        var index = $(e.target).index();
        if (this.state.prev <= index) {
            $(e.target).parent().children().each(
                function (i) {
                    if (i <= index)
                        $(this).addClass('is-full')
                }
            );
            $(e.target).addClass('is-full');
            if (!$(e.target).is(':first-child')) {
                $(e.target).prev().addClass('is-full');
            }
        } else {
            $(e.target).next().removeClass('is-full');
        }
        this.setState({prev: index});
    }

    vote(e) {
        if (isAuthenticated)
            this.props.vote($(e.target).index() + 1);
        else {
            ReactTestUtils.Simulate.click($('.nav-buttons .btn-default:last-child').get(0));
        }
    }

    render() {
        var stars = [];
        for (var i = 1; i <= 5; i++) {
            if (this.props.rate != undefined) {
                if (i <= this.props.rate) {
                    stars.push(
                        <b className='fa fa-star' key={i}/>
                    );
                } else {
                    if (i - 0.5 == this.props.rate) {
                        stars.push(
                            <b className='fa fa-star-half-o' key={i}/>
                        );
                    } else
                        stars.push(
                            <b className='fa fa-star-o' key={i}/>
                        );
                }
            } else {
                stars.push(
                    <b className='star fa fa-circle' key={i}
                       onMouseEnter={e => this.handleSingleHover(e)}
                       onClick={e => this.vote(e)}
                    />
                );
            }
        }
        return (
            <div className='stars'
                 onMouseEnter={this.props.rate == undefined ? this.handleHover : ''}
                 onMouseLeave={this.props.rate == undefined ? this.handleLeave : ''}>
                {stars}
            </div>
        )
    }
}
