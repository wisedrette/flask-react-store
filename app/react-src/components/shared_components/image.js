import React from 'react'

export default class Image extends React.Component {

    handleLoadError(event) {
        var img = this.props.wide ? '/static/news_placeholder.jpg' : '/static/bottle_placeholder.png';
        $(event.target).attr('src', img);
    }

    render() {
        var path = /^(\/)?static/.test(this.props.src) ? this.props.src : '/static/'+this.props.src;
        return (
            <img className="img-responsive" src={path} onError={e => this.handleLoadError(e)}/>
        )
    }
}
