import React from 'react';
import { Col } from 'react-bootstrap';
import Image from './image'

var OrderListItem = React.createClass({

    getBodyList(){
        return this.props.order.beers.map(beer =>
            <div className='item history' key={beer.id}>
                <Col xs={2} className='image'>
                    <Image src={'/static/'+beer.img_path} responsive/>
                </Col>
                <Col xs={6} className='info pull-left'>
                    <span>
                        {beer.name}
                    </span>
                </Col>
                <Col xs={2} className='counter'>
                    <span className='pull-right'>
                            <i className='fa fa-times'/>{beer.count}
                    </span>
                </Col>

                <div className='pull-right price'>{'$' + beer.price}</div>
            </div>);
    },

    getHeaderPulledRight(){
        if (this.props.isAdmin) {
            return (
                <div>
                    <a className='btn btn-default pull-right'
                       onClick={() => this.props.confirm(this.props.order.id)}>
                        CONFIRM
                    </a>
                </div>
            )
        } else {
            var status, statusColor;

            if (this.props.order.delivered) {
                status = 'delivered';
                statusColor = ' success';
            } else if (this.props.order.confirmed_at == null) {
                status = 'waiting for confirmation';
                statusColor = ' waiting';
            } else
                status = 'waiting for delivery';

            return (
                <div className={'info-status'+ statusColor}>
                    <b>status: </b>{status}
                </div>
            )
        }

    },


    getHeader() {
        return (
            <div className='panel-heading pull-left'>
                <Col className='text-left' xs={12} sm={7} md={6} lg={7}>
                    <div className='info-main'>
                        {'Order #' + this.props.order.uid}
                    </div>
                    <div className='info-date'>
                        <small className='text-left'>{timeConverter(this.props.order.added_at)}</small>
                    </div>
                </Col>
                <Col className='pull-right text-left' xs={12} sm={5} md={6} lg={5}>
                    {this.getHeaderPulledRight()}
                </Col>
            </div>
        )
    },

    getFooter(){
        if (this.props.isAdmin) {
            return (
                <div className='item user-info'>
                    <Col md={6} xs={12}>
                        <small>
                            <b>Username: </b>
                            {this.props.order.user.username}
                        </small>
                        <br/>
                        <small>
                            <b>Email: </b>
                            {this.props.order.user.email}
                        </small>
                    </Col>
                    <Col md={6} xs={12}>
                        <small>
                            <b>Phone number: </b>
                            {this.props.order.phone}
                        </small>
                    </Col>
                </div>
            )

        }
    },

    render() {
        var total = '$' + this.props.order.total.toFixed(2);
        return (
            <div className='panel panel-default order-item'>
                {this.getHeader()}
                <div className='panel-body'>
                    {this.getBodyList()}
                    <div className='item total'>
                        <Col sm={7}>
                            <small>
                                <b>Delivery address: </b>
                                {this.props.order.address}
                            </small>
                        </Col>
                        <span className='pull-right'>{total}</span>
                    </div>
                    {this.getFooter()}
                </div>
            </div>
        )
    }
});


function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
}

export default OrderListItem;