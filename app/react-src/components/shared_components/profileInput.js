import React from 'react';

var ProfileInput = React.createClass({

    render() {
        var actionClassName = this.props.type == 'value' ? 'btn-default' : 'btn-success';
        var actionBtnText = this.props.type == 'value' ? 'CHANGE' : 'SAVE';

        var action = this.props.action ?
            <span className={'btn '+actionClassName} onClick={this.props.onSave}>{actionBtnText}</span> : '';
        var statusMessages = [
            ['processing', ' fa-spinner fa-spin'],
            ['result', ' fa-check-circle'],
            ['error', ' fa-warning']].map((status, i) =>
                <span className={status[0]} key={i}>
                    <i className={'fa'+status[1]} style={{display: 'none'}}/>
                </span>);

        var wrapperClassName = this.props.type == 'value' ? 'value' : 'input-wrapper';

        return (
            <div className={'form-group '+this.props.label}>
                <label className='control-label col-xs-2'>
                    <span>{this.props.label}</span>
                </label>

                <div className={wrapperClassName + ' col-xs-6'}>
                    {this.props.children}
                </div>
                <span className='col-xs-4 action'>
                    {statusMessages}
                    {action}
                </span>
            </div>
        )
    }
});


export default ProfileInput;