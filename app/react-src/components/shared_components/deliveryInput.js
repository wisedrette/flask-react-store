import React from 'react';

var validators = {
    address: /.*/,
    phone: /([+]?)(\d{6,12})/,
    zip: /(\d{5})/
};

var DeliveryInput = React.createClass({
    getInitialState() {
        return {
            value: this.props.value
        }
    },

    getValue(){
        return this.state.value == null ? '' : this.state.value;
    },

    setValue(value){
        this.setState({value: value});
    },

    toggleFieldError(isHidden){
        var inputSelector = $(this.refs.input);
        if (isHidden)
            $(inputSelector).removeClass('has-error');
        else
            $(inputSelector).addClass('has-error');
    },

    validate(){
        var input = $(this.refs.input).val();
        if (this.props.name == 'phone')
            input = input.replace(/\D/g, '');
        return validators[this.props.name].test(input);
    },

    handleChange(event){
        if (this.validate())
            this.toggleFieldError(this.validate());

        this.setState({value: event.target.value});
    },

    handleBlur() {
        this.toggleFieldError(this.validate());
        if (this.props.onBlur != undefined)
            this.props.onBlur(this.props.name);
    },

    componentDidUpdate(prevProps, prevState){
        var name = this.props.name;
        if (prevProps.value != this.props.value)
            this.setState({value: this.props.value});
        if (prevState.value != this.state.value && this.props.onChange != undefined && prevState.value != null)
            this.props.onChange(name);
    },

    render() {
        return (
            <input type='text' placeholder={this.props.placeholder} name={this.props.name} ref='input'
                   value={this.state.value} onChange={this.handleChange}
                   onBlur={this.handleBlur}
                   maxLength={this.props.length} className='form-control'/>
        )
    }
});


export default DeliveryInput;