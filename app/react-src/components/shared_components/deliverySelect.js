import React from 'react';


var DeliverySelect = React.createClass({
    getInitialState() {
        return {states: [], value: this.props.value}
    },

    getValue(){
        return this.state.value;
    },

    validate() {
        return this.state.value != '' && this.state.value != 0;
    },

    setValue(value){
        this.setState({value: value});
    },

    componentWillMount() {
        $.ajax({
            url: '/api/states',
            dataType: 'json',
            cache: false,
            success: (data => this.setState(data))
        });
    },

    componentDidUpdate(prevProps, prevState){
        if (prevProps.value != this.props.value)
            this.setState({value: this.props.value});
        if (prevState.value != this.state.value && prevState.value != null)
            this.props.onChange(this.props.name)
    },

    handleChange(event){
        this.setState({
            value: event.target.value
        });
    },


    render () {
        var states = this.state.states.map(state => <option value={state.code} key={state.code}>{state.name}</option>);
        states.unshift(<option value='0' key={0} disabled>-- SELECT STATE --</option>);

        return (
            <select type='select' className='form-control' value={this.state.value} onChange={this.handleChange}>
                {states}
            </select>
        );
    }
});

export default DeliverySelect;