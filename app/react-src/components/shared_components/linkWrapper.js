import React from 'react'
import { Link } from 'react-router'
import ReactTestUtils from 'react-addons-test-utils'

export default class LinkWrapper extends React.Component {

    handleClick(e) {
        if (!this.props.checkAuthorization || this.props.checkAuthorization == undefined)
            window.scrollTo(0, 0);
        else {
            if (isAuthenticated)
                window.scrollTo(0, 0);
            else {
                e.preventDefault();
                e.stopPropagation();
                ReactTestUtils.Simulate.click($('.nav-buttons .btn-default:last-child').get(0));
            }
        }
    }

    render() {
        return (
            <Link to={this.props.to} className={this.props.className == undefined ? '' : this.props.className}
                  activeClassName={this.props.activeClassName == undefined ? '' : this.props.activeClassName}
                  onMouseLeave={this.props.onMouseLeave}
                  onMouseEnter={this.props.onMouseEnter}
                  onClick={e => this.handleClick(e)}>
                {this.props.children}
            </Link>
        )
    }
}