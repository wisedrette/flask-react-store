import React from 'react';
import { Input } from 'react-bootstrap'

export default class InputField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {validationError: false};
        this.state.value = props.value == undefined ? '' : props.value;

        this.validate = this.validate.bind(this);

        if (props.regex == undefined) {
            switch (props.dataType) {
                case 'email':
                    this.regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    this.helpMsg = 'Invalid email format';
                    break;
                case 'username':
                    this.regex = /^[A-Za-z][A-Za-z0-9_.]*$/;
                    this.helpMsg = 'Usernames must have only letters, numbers, dots or underscores';
                    break;
                case 'password':
                    this.regex = /(.){4,}/;
                    this.helpMsg = 'Password is too short';
                    break;
                default:
                    this.regex = /(.)+/;
                    this.helpMsg = 'This field is required';
            }
        } else
            this.regex = props.regex;

        if (props.helpMsg != undefined)
            this.helpMsg = props.helpMsg;
    }

    getValue() {
        return this.refs.input.getValue();
    }

    resetValue() {
        var value = this.props.value == undefined ? '' : this.props.value;
        this.setState({value: value});
    }

    validate() {
        var result = this.regex.test(this.refs.input.getValue());
        this.setState({validationError: !result});
        return result
    }

    setValidationError(message) {
        this.helpMsg = message;
        this.setState({validationError: true});
    }

    handleChange() {
        var state = {};
        state.value = this.refs.input.getValue();
        if (this.state.validationError)
            state.validationError = false;
        this.setState(state);
    }

    validateHelpMessage() {
        if (this.state.validationError)
            if (this.refs.input.getValue().length == 0)
                return 'This field is required';
            else
                return this.helpMsg;
    }

    validateState() {
        if (this.state.validationError)
            return 'error';
    }

    componentWillReceiveProps(newProps) {
        if (newProps.value != this.props.value)
            this.setState({value: newProps.value})
    }

    render() {
        return (
            <Input type={this.props.type}
                   placeholder={this.props.placeholder}
                   label={this.props.label}
                   value={this.state.value}
                   group-class-name='form-control'
                   help={this.validateHelpMessage()}
                   bsStyle={this.validateState()}
                   ref='input'
                   onChange={() => this.handleChange()}
            />
        )
    }
}