import React from 'react'

export default class RuFormattedPlural extends React.Component {
    render() {
        var text;
        if (this.props.value > 5 && this.props.value < 19)
            text = this.props.many;
        else if (this.props.value % 10 == 1)
            text = this.props.one;
        else if ([2, 3, 4].indexOf(this.props.value % 10) != -1 )
            text = this.props.few;
        else
            text = this.props.many;
        return (
            <span>{text}</span>
        )
    }
}
