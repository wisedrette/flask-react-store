import React from 'react';
import CardInput from './cardInput.js'
import CardSelect from './cardSelect.js'

export default class CardForm extends React.Component{
    constructor() {
        super();
        this.validateSelect = this.validateSelect.bind(this);
        this.checkFields = this.checkFields.bind(this);
    }

    checkFields(){
        var filled = true;
        Object.keys(this.refs).forEach(function (key) {
            var check = this.refs[key].validate();
            filled = filled && check;
        }.bind(this));
        return filled;
    }

    validateSelect(){
        var currentDate = new Date();
        var expiryDate = new Date();
        expiryDate.setFullYear(this.refs.year.getValue(), this.refs.month.getValue() - 1, 1);
        var isExpired = currentDate <= expiryDate;
        if (this.refs.year.getValue() != 0)
            this.refs.month.toggleFieldError(isExpired);
        return isExpired;
    }

    render() {
        return (
            <form action='' method='POST' id='payment-form' className='payment-form'>
                <span className='payment-errors' />

                <div className='form-group input-wrapper'>
                    <CardInput placeholder='Card number' dataStripe='number'
                               ref='number' onChange={this.props.onChange}/>
                </div>

                <div className='form-group'>
                    <div className='input-wrapper month'>
                        <CardSelect dataStripe='exp-month' placeholder={0}
                                    ref='month' onChange={this.props.onChange} validate={this.validateSelect}/>
                    </div>
                    <div className='input-wrapper year'>
                        <CardSelect dataStripe='exp-year' placeholder={0}
                                    ref='year' onChange={this.props.onChange} validate={this.validateSelect}/>
                    </div>
                    <div className='input-wrapper cvc'>
                        <CardInput size='4' dataStripe='cvc' placeholder='CVV2'
                                   ref='cvc' onChange={this.props.onChange}/>
                    </div>
                </div>
            </form>
        )
    }
}
