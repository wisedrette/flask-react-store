import React from 'react';

var CardSelect = React.createClass({
    getInitialState() {
        return {
            value: ''
        }
    },

    validate() {
        return this.props.validate()
    },

    getValue(){
        return this.state.value;
    },

    handleChange(event){
        this.setState({value: event.target.value});
    },

    toggleFieldError(isHidden){
        var inputSelector = $(this.refs.select);
        if (isHidden)
            $(inputSelector).removeClass('has-error');
        else
            $(inputSelector).addClass('has-error');
    },

    componentDidUpdate(prevProps, prevState){
        if (prevState.value != this.state.value)
            this.props.onChange()
    },

    render() {
        var optionsList;

        if (this.props.dataStripe == 'exp-month') {
            var months = ['Expiry Month', 'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'];

            optionsList = months.map((month, i) =>
                <option value={i} disabled={i==0} key={i}>{i == 0 ? month : i + ' - ' + month}</option>);

        } else {
            var currentYear = new Date().getFullYear();
            var years = Array.from(new Array(22), (x, i) => i + currentYear);
            years.unshift('Year');

            optionsList = years.map((year, i) =>
                <option value={/\d+/.test(year) ? year : 0} disabled={i==0} key={i}>{year}</option>);
        }

        return (
            <select data-stripe={this.props.dataStripe} defaultValue={this.props.placeholder}
                    className='form-control' onChange={this.handleChange} ref='select'>
                {optionsList}
            </select>
        )
    }
});


export default CardSelect;