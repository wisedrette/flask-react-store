import React from 'react';

var validators = {
    number: /((\d{4}\s?){4,})/,
    cvc: /(\d{3}\d?)/
};

var CardInput = React.createClass({
    getInitialState() {
        return {
            value: ''
        }
    },

    getValue(){
        return this.state.value;
    },

    validate() {
        return validators[this.props.dataStripe].test($(this.refs.input).val());
    },

    toggleFieldError(isHidden){
        var inputSelector = $(this.refs.input);
        if (isHidden)
            $(inputSelector).removeClass('has-error');
        else
            $(inputSelector).addClass('has-error');
    },

    handleChange(event){
        var value = event.target.value;
        if (this.props.dataStripe == 'number') {
            value = value.replace(/\s/g, '');
            value = value.replace(/(\d{4})/g, '$1 ').trim();
        }
        if (this.validate())
            this.toggleFieldError(this.validate());
        this.setState({value: value});
    },

    componentDidUpdate(prevProps, prevState){
        if (prevState.value != this.state.value)
            this.props.onChange()
    },

    render() {
        return (
            <input type='text' size={this.props.size} value={this.state.value}
                   data-stripe={this.props.dataStripe} placeholder={this.props.placeholder}
                   className='form-control' ref='input' maxLength={this.props.dataStripe == 'cvc' ? 4 : 24}
                   onChange={this.handleChange} onBlur={() => this.toggleFieldError(this.validate())}
                />
        )
    }
});


export default CardInput;