import React from 'react';
import { DeliveryInput, DeliverySelect, locationCheck } from '../shared_components/'

var DeliveryCheckoutForm = React.createClass({
    getInitialState() {
        return {save: false}
    },

    checkFields(){
        var filled = true;
        Object.keys(this.refs).forEach(key => filled = filled && this.refs[key].validate());
        return filled;
    },

    getValue() {
        return {
            address: this.refs.address.getValue(),
            state: this.refs.state.getValue(),
            zip: this.refs.zip.getValue(),
            phone: this.refs.phone.getValue(),
            save: this.state.save
        }
    },

    handleChange(field){
        locationCheck(this.refs.state.getValue(), this.refs.address.getValue(), this.refs.zip.getValue(), function (status, result) {
            if (status) {
                if (result.message != '')
                    this.addressMessageWarning(result.message);
                Object.keys(result.result).forEach(key => this.refs[key].setValue(result.result[key]));
                this.refs.address.toggleFieldError(true);
                this.refs.zip.toggleFieldError(true);
                this.props.onChange();
            } else {
                if (field == 'state') {
                    this.refs.address.setValue('');
                    this.refs.zip.setValue('');
                }
                this.refs.address.toggleFieldError(false);
                this.refs.zip.toggleFieldError(false);
                this.addressMessageWarning(result.message);
            }
        }.bind(this));
    },

    addressMessageWarning(message){
        var classSelection = '.payment-errors';
        $(classSelection).find('.fa').show().text(' ' + message)
            .delay(1500).fadeOut('slow', function () {
                $(classSelection).find('.fa').hide().text('');
            });
    },


    render() {
        var checkBox;
        if (isAuthenticated)
            checkBox =
                <label>
                    <input type="checkbox" value="" onChange={() => this.setState({save: !this.state.save})}/><span>Save delivery details</span>
                </label>;
        return (
            <form action='' method='POST' id='payment-form' className='payment-form'>
                <div className='payment-errors'><b className='fa fa-warning' style={{display: 'none'}}/></div>

                <div className='form-group input-wrapper'>
                    <DeliveryInput placeholder='Delivery address' name='address'
                                   value={this.props.user==null ? '' : this.props.user.address}
                                   ref='address' onBlur={this.handleChange}/>
                </div>

                <div className='form-group'>
                    <div className='input-wrapper state'>
                        <DeliverySelect placeholder={0} name='state'
                                        value={this.props.user==null ? '' : this.props.user.state}
                                        ref='state' onChange={this.handleChange}/>
                    </div>
                    <div className='input-wrapper zip'>
                        <DeliveryInput placeholder='ZIP' name='zip'
                                       value={this.props.user==null ? '' : this.props.user.zip}
                                       ref='zip' length={5} onChange={this.props.onChange}/>
                    </div>
                </div>

                <div className='form-group'>
                    <DeliveryInput placeholder='Phone number' name='phone'
                                   value={this.props.user==null ? '' : this.props.user.phone}
                                   ref='phone' length={13} onChange={this.props.onChange}/>
                </div>

                <div className='delivery-save'>{checkBox}</div>
            </form>
        );
    }
});

export default DeliveryCheckoutForm;