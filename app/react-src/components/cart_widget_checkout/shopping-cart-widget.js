import React from 'react';
import { Row, Col, NavDropdown, NavItem, MenuItem, Image, Badge, Glyphicon, Button } from 'react-bootstrap'
import CartItem from './subcomponents/shopping-cart-item.js'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { addBeer, deleteBeer } from '../../redux/actions/actions.js'
import { FormattedNumber, FormattedMessage, FormattedPlural } from 'react-intl';
import { RuFormattedPlural } from '../shared_components'
import { messages } from '../../helper_functions/intlMessages.js'

class CartWidget extends React.Component {
    constructor() {
        super();
        this.toggleCollapsedWidget = this.toggleCollapsedWidget.bind(this);
        this.closeWidget = this.closeWidget.bind(this);
    }

    toggleCollapsedWidget(event) {
        if (this.props.collapsed) {
            var contentWrapper = $('.content-wrapper');
            if (/navbar-expanded/.test(contentWrapper.attr('class'))) {
                $('.navbars > .navbar-nav.collapsed').removeClass('is-expanded').addClass('is-contracted');
                contentWrapper.removeClass('navbar-expanded');
            }
            contentWrapper.toggleClass('widget-expanded');

        }
        var parent = $(event.target).closest('.dropdown-cart');
        parent.toggleClass('is-contracted is-expanded');
        parent.find('.dropdown-container').toggleClass('in');
    }

    closeWidget(event) {
        var parent = $(event.target).closest('.dropdown-cart');
        parent.removeClass('is-expanded').addClass('is-contracted');
        parent.find('.dropdown-container').removeClass('in');
        $('.content-wrapper').removeClass('widget-expanded');
    }


    render() {
        const { beerList, dispatch } = this.props;
        var count = 0;
        var baseClassName = 'dropdown-cart is-contracted';
        if (this.props.collapsed)
            baseClassName += ' collapsed';

        if (this.props.checkout || beerList.beers.length == 0) {
            if (beerList.beers.length != 0)
                beerList.beers.forEach(beer => count += beer.count);


            return (
                <li className={baseClassName + ' disabled'}>
                    <WidgetTitle count={count} triangle={!this.props.collapsed}/>
                </li>
            )

        } else {
            var total = 0.0;
            var items = beerList.beers.map(function (beer, i) {
                count += beer.count;
                total += beer.price;

                if (i < beerList.beers.length && i > beerList.beers.length - 6)
                    return <CartItem beer={beer} key={i} img={!this.props.collapsed}
                                     onAdd={ beer => dispatch(addBeer(beer)) }
                                     onDelete={ id => dispatch(deleteBeer(id)) }/>
            }.bind(this));

            return (
                <li className={baseClassName}>
                    <WidgetTitle count={count} triangle={!this.props.collapsed}
                                 onClick={this.toggleCollapsedWidget}/>

                    <div className='dropdown-container'>
                        <WidgetHeader count={count} total={total} collapsed={this.props.collapsed}
                                      onClick={this.closeWidget}/>
                        {items}
                    </div>
                </li>
            )
        }
    }
}

class WidgetTitle extends React.Component {
    render() {
        var triangle = this.props.triangle ? <i className='glyphicon glyphicon-triangle-top'/> : '';

        return (
            <a role='button' aria-haspopup='true' aria-expanded='false'
               className='extras' onClick={this.props.onClick}>
                <div>
                    <i className='fa fa-shopping-cart fa-flip-horizontal'/>
                    <div className='count'>{this.props.count}</div>
                </div>
                {triangle}
            </a>
        )
    }
}

class WidgetHeader extends React.Component {
    render() {
        var counter;
        if (localStorage.getItem('lang') == 'ru')
            counter = (
                <span>
                    <FormattedNumber value={this.props.count}/> {' '}
                    <RuFormattedPlural value={this.props.count}
                                     one="продукт" few="продукта"
                                     many="продуктов" other="продуктов"/>
                </span>
            );
        else
            counter = (
                <span>
                    <FormattedNumber value={this.props.count}/> {' '}
                    <FormattedPlural value={this.props.count}
                                     one="item"
                                     other="items"/>
                </span>
            );
        if (this.props.collapsed)
            return (
                <div className='header'>
                    <Link to={'/checkout'} className='btn btn-success checkout'
                          onClick={() => this.props.onClick()}>
                        <FormattedMessage  {...messages.checkout}/>
                    </Link>

                    {counter}
                    <i className='fa fa-circle'/>
                    <span className='price'>
                        <FormattedNumber value={this.props.total} style="currency" currency="USD"/>
                    </span>

                </div>
            );

        else
            return (
                <div className='header'>
                    {counter}
                    <span className='pull-right'>
                        <span className='price'>
                            <FormattedNumber value={this.props.total} style="currency" currency="USD"/>
                        </span>
                        <Link to={'/checkout'} className='btn btn-success checkout pull-right'
                              onClick={this.props.onClick}>
                            <FormattedMessage {...messages.checkout} />
                        </Link>
                    </span>
                </div>
            );
    }
}


function select(state) {
    return {
        beerList: state.beerList
    }
}

export default connect(select)(CartWidget);


