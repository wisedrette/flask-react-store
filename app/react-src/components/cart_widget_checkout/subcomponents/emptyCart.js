import React from 'react'

export default class EmptyCart extends React.Component {
    render() {
        return (
            <div className='order'>
                <div className='left'>
                    <h4>YOUR ORDER</h4>
                    <div className='item-list empty'>
                        Your order is empty
                    </div>
                </div>
            </div>
        )
    }
}