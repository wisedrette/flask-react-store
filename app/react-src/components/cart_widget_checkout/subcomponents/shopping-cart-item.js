import React from 'react';
import { Col, Row, Glyphicon } from 'react-bootstrap'
import Image from '../../shared_components/image.js'
import { FormattedNumber } from 'react-intl';


var CartItem = React.createClass({
    render(){
        var counter, image, infoClass = '';
        if (this.props.beer.count > 1) {
            counter =
                <div className='counter'>
                    <span className='pull-right'><i className='fa fa-times'/>{this.props.beer.count}</span>
                </div>;
        } else
            infoClass = ' alone';

        if (this.props.img || this.props.img == undefined)
            image = (
                <div className='image'>
                    <Image src={this.props.beer.img_path}/>
                </div>
            );

        return (
            <div className='item'>
                {image}
                <div className='info-wrapper'>
                    <div className={'info'+infoClass}>
                        <span>{this.props.beer.name}</span>
                        <small className='info-hovered noselect'>
                            <a onClick={() => this.props.onAdd(this.props.beer)}>Add another</a>
                            <a onClick={() => this.props.onDelete(this.props.beer.id)}>Remove</a>
                        </small>
                    </div>
                    {counter}

                    <div className='price'>
                        <FormattedNumber value={this.props.beer.price} style="currency" currency="USD"/>
                    </div>
                </div>
            </div>
        )
    }
});

export default CartItem;