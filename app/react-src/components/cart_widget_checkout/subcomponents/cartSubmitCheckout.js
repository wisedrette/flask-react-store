import React from 'react';
import { Panel } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl';


export default class SubmitCheckoutPanel extends React.Component {
    render() {
        return (
            <Panel className='payment-submit'>
                <span className='pull-left title'>
                    <FormattedNumber value={this.props.total} style="currency" currency="USD"/>
                </span>
                <span className='submit'>
                    <a className='btn btn-success pull-right disabled' onClick={() => this.props.placeOrder()}>
                        PAY FOR YOUR ORDER NOW
                    </a>
                </span>
            </Panel>
        )
    }
}
