import React from 'react';
import CartItem from './shopping-cart-item.js'
import { FormattedNumber } from 'react-intl';

export default class OrderDetails extends React.Component {
    render() {
        var items = this.props.beers.map((beer, i) =>
            <CartItem beer={beer} key={i}
                      onAdd={ beer => this.props.addBeer(beer) }
                      onDelete={ id => this.props.deleteBeer(id) }/>
        );

        var discount;
        if (this.props.discount != null && this.props.discount != 0)
            discount = (
                <div className='item discount'>
                    <label>
                        <input type="checkbox" value="" onChange={() => this.props.applyDiscount()}/>
                        <span>Apply {this.props.discount}% discount</span>
                    </label>
                </div>
            );
        return (
            <section className='item-list'>
                {items}
                <div className='item total'>
                    <div className='col-sm-7'>
                        <small>
                            All prices include VAT if applicable.
                        </small>
                    </div>
                    <span className='total'>ORDER TOTAL:&nbsp;
                        <FormattedNumber value={this.props.total} style="currency" currency="USD"/>
                    </span>
                </div>
                {discount}
            </section>
        )

    }
}