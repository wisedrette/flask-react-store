import React from 'react'
import { PanelGroup, Panel } from 'react-bootstrap'
import CardForm from './../card/cardForm.js'


var PayPalHeader = <span><i className='fa fa-lg fa-cc-paypal'/>PayPal</span>;
var CardHeader =
    <span>
        <i className='fa fa-lg fa-cc-visa'/><i className='fa fa-lg fa-cc-mastercard'/><i
        className='fa fa-lg fa-cc-amex'/>
        Debit/Credit card
    </span>;

export default class CardPanel extends React.Component {
    constructor() {
        super();
        this.state = {activeKey: '1'}
    }

    handleSelect(activeKey) {
        if (activeKey == this.state.activeKey)
            this.setState({activeKey: ''});
        else
            this.setState({activeKey})
    }

    checkFields(){
        return this.refs.cardForm.checkFields()
    }

    render() {
        return (
            <PanelGroup activeKey={this.state.activeKey} onSelect={(key) => this.handleSelect(key)} accordion>
                <Panel header={CardHeader} eventKey='1'>
                    <CardForm ref='cardForm' onChange={() => this.props.checkFields()}/>
                </Panel>


                <Panel header={PayPalHeader} eventKey='2'>
                    <div className='text-center'>
                        After clicking 'Complete order now', you will be redirected to the PayPal page to finish
                        your transaction
                    </div>
                </Panel>

            </PanelGroup>
        )
    }
}
