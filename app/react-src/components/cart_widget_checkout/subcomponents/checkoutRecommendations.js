import React from 'react';
import ContentList from '../../content/content_list/contentList.js'


export default class CheckoutRecommendations extends React.Component {
    constructor(props){
        super(props);
        this.state = {data: []}
    }

    getRecommendations() {
        var ids = this.props.added.map(beer => beer.id).join();
        $.ajax({
            method: 'GET',
            url: '/api/similar/orders/' + ids,
            contentType: 'application/json',
            success: (data => this.setState(data))
        })
    }

    componentDidMount(){
        this.getRecommendations();
    }

    componentDidUpdate(prevProps){
        if (prevProps.added.length != this.props.added.length)
            this.getRecommendations();
    }

    addToCart(beer){
        this.props.addToCart(beer);
        this.getRecommendations();
    }

    render(){
        if (this.state.data.length > 0)
            return (
                <div className='recommendations index-products'>
                    <h4 className='upper'>You might also like</h4>
                    <ContentList data={this.state.data} view='grid' added={this.props.added}
                                 addToCart={ beer => this.addToCart(beer) }/>
                </div>
            );
        else
            return <div className='recommendations'></div>;
    }
}