import React from 'react';
import { Panel } from 'react-bootstrap'

import { CartItem, CardPanel, CheckoutRecommendations, EmptyCart, OrderDetails, SubmitCheckoutPanel } from './subcomponents/'
import DeliveryCheckoutForm from './deliveryCheckoutForm'

import { connect } from 'react-redux'
import { addBeer, deleteBeer, clearCart } from '../../redux/actions/actions.js'


class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                address: null,
                email: null,
                id: null,
                phone: null,
                avatar: null,
                state: null,
                username: null,
                zip: null,
                discount: null
            },
            data: [],
            useDiscount: false
        };
    }

    componentWillMount() {
        $.ajax({
            method: 'GET',
            url: '/api/account',
            contentType: 'application/json',
            success: (data => this.setState(data))
        });
    }

    checkFields() {
        if (this.refs.cardPanel.checkFields() && this.refs.deliveryForm.checkFields())
            $('.submit > .btn').removeClass('disabled');
        else
            $('.submit > .btn').addClass('disabled');
    }

    placeOrder() {
        var details = this.refs.deliveryForm.getValue();
        details.total = 0;
        details.beers = this.props.beerList.beers.map(function (beer) {
            details.total += beer.price * beer.count;
            return {id: beer.id, count: beer.count}
        });

        var save = details.save;
        delete details.save;

        var discount = this.state.useDiscount ? this.state.user.discount : 0;
        if (discount != 0) {
            var total = details.total * (1 - discount / 100);
            details.total = parseFloat(total.toFixed(2));
        }


        $.ajax({
            method: 'POST',
            url: '/api/checkout',
            contentType: 'application/json',
            data: JSON.stringify({order: details, save: save, discount: discount}),
            success: function () {
                localStorage.setItem('cartList', JSON.stringify([]));
                location.replace('/index');
            }.bind(this),
            error: function (data) {
            }
        });
    }

    getTotal() {
        var total = 0.0;

        this.props.beerList.beers.forEach(function (beer) {
            total += beer.price * beer.count;
        });
        if (this.state.useDiscount){
            total = total*(1-this.state.user.discount/100)
        }

        return total;
    }

    render() {
        if (this.props.beerList.beers.length > 0)
            return (
                <div className='order'>
                    <div className='left'>
                        <h4>YOUR ORDER</h4>
                        <OrderDetails beers={this.props.beerList.beers} total={this.getTotal()}
                                      addBeer={ beer => this.props.dispatch(addBeer(beer)) }
                                      deleteBeer={ id => this.props.dispatch(deleteBeer(id)) }
                                      applyDiscount={() => this.setState({useDiscount: !this.state.useDiscount})}
                                      discount={this.state.user.discount} />
                    </div>

                    <div className='right payment'>
                        <h4>YOUR PAYMENT &amp; SHIPPING DETAILS</h4>

                        <CardPanel ref='cardPanel' checkFields={() => this.checkFields()}/>

                        <Panel header='Delivery details'>
                            <DeliveryCheckoutForm user={this.state.user} ref='deliveryForm'
                                                  onChange={() => this.checkFields()}/>
                        </Panel>

                        <SubmitCheckoutPanel total={this.getTotal()} placeOrder={() => this.placeOrder()}/>
                    </div>

                    <div className='small-info'>
                        We never save your payment details and the order is protected by secure connection. Diam eget
                        etiam. Torquent placerat eu. Aenean eget mollis nonummy non leo eu sed vestibulum. Sed egestas
                        nulla convallis vivamus tempora ac turpis nullam. Ullamcorper etiam eleifend. Ut ipsum proin
                        penatibus feugiat nulla bibendum porttitor sapien. Need help? Check our help pages or
                        contact us.
                    </div>

                    <CheckoutRecommendations added={this.props.beerList.beers}
                                             addToCart={ beer => this.props.dispatch(addBeer(beer)) }/>
                </div>
            );
        else
            return <EmptyCart />
    }
}


function select(state) {
    return {
        beerList: state.beerList
    }
}

export default connect(select)(Cart);
