import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../../shared_components/inputField.js'

export default class EmailChangeModal extends React.Component{

    serialize() {
        return JSON.stringify({email: this.refs.email.getValue()})
    }

    changeEmail(event){
        var passed = this.refs.email.validate();

        if (passed) {
            $.ajax({
                method: 'POST',
                url: '/api/account/email_change',
                contentType: 'application/json',
                data: this.serialize(),
                success: function (data) {
                    console.log(data);
                    if (data.saved) {
                        this.props.updateData();
                        this.props.close();
                    } else {
                        this.refs.email.setValidationError('Email already in use');
                    }
                }.bind(this)
            });
        }
        $(event.target).blur();
    }

    handleFormSubmit(event){
        event.preventDefault();
        this.changeEmail(event)
    }

    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton />
                <Modal.Body>
                    <h4 className='title'>account</h4>
                    <div className='center-block text-center user-info'>
                        <h3 className='username'>{this.props.user.username}</h3>

                        <p className='email'>{this.props.user.email}</p>
                    </div>
                    <form method='post' id='emailForm' onSubmit={() => this.handleFormSubmit()}>
                        <p className='help-block'></p>

                        <div className='form-group'>
                            <InputField type='text' placeholder='New email'
                                        ref='email' dataType='email' />
                        </div>
                        <Button bsStyle='success' id='submit_reset' onClick={() => this.changeEmail()} block>
                            Save new email
                        </Button>
                    </form>
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showProfileModal')}>
                            Go back
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}