import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../../shared_components/inputField.js'

var ProfileModal = React.createClass({

    getInitialState() {
        return {imageError: false}
    },

    serialize() {
        return JSON.stringify({username: this.refs.username.getValue(), password: this.refs.password.getValue()})
    },

    selectImage(){
        this.setState(this.getInitialState());
        $('#file').click();
    },

    uploadImage(event){
        var file = event.target.files[0];
        if (/image/.test(file.type)) {
            var form = document.forms.namedItem("files");
            var formData = new FormData(form);
            var req = new XMLHttpRequest();
            req.open("POST", "/api/account/img_change", true);
            req.onload = function () {
                if (req.status == 200) {
                    this.props.updateData();
                    this.setState(this.getInitialState());
                    this.props.close();
                } else {
                    this.setState({pwdCheckError: 'Something went wrong'});
                }
            }.bind(this);

            req.send(formData);
        }
        else
            this.setState({imageError: 'Please upload valid image'})
    },


    render() {

        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton/>
                <Modal.Body>
                    <h4 className='title'>account</h4>

                    <div className='center-block text-center user-info'>
                        <div className='avatar'>
                            <i className='fa fa-camera'/>

                            <div className='avatar-holder'>
                                <img src={this.props.user.avatar} className='avatar-img' alt='avatar'
                                     onClick={this.selectImage} onLoad={this.props.imageCentering}/>
                            </div>
                        </div>
                        <form encType="multipart/form-data" method="post" name="files" id='files'
                              style={{display: 'none'}}>
                            <input type="file" id='file' name="file" onChange={this.uploadImage}/>
                        </form>
                        <p className='help-block has-error'>{this.state.imageError}</p>

                        <h3 className='username'>{this.props.user.username}</h3>

                        <p className='email'>{this.props.user.email}</p>
                    </div>
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showEmailChangeModal')}>
                            Change email address
                        </a>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showPasswordChangeModal')}>
                            Change password
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
});

export default ProfileModal;