import React from 'react';
import { Link } from 'react-router'
import {  Modal, Button } from 'react-bootstrap'
import InputField from '../../../shared_components/inputField.js'


export default class PasswordChangeModal extends React.Component{

    serialize() {
        return JSON.stringify({old_password: this.refs.oldPassword.getValue(), password: this.refs.password.getValue()})
    }

    changePassword(){
        var passed = true;
        Object.keys(this.refs).forEach( key => passed = this.refs[key].validate() && passed );
        if (this.refs.password.getValue() != this.refs.repeat.getValue()) {
            this.refs.password.setValidationError('Passwords don\'t match');
            this.refs.repeat.setValidationError('Passwords don\'t match');
            passed = false;
        }
        if (passed) {
            $.ajax({
                method: 'POST',
                url: '/api/account/pwd_change',
                contentType: 'application/json',
                data: this.serialize(),
                success: function (data) {
                    if (data.saved) {
                        this.props.close();
                        this.setState(this.getInitialState());
                    } else {
                        this.refs.oldPassword.setValidationError('Wrong old password');
                    }
                }.bind(this)
            });
        }
        $(event.target).blur();
    }

    handleFormSubmit(event){
        event.preventDefault();
        this.changePassword(event)
    }


    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton/>
                <Modal.Body>
                    <h4 className='title'>account</h4>

                    <div className='center-block text-center user-info'>
                        <h3 className='username'>{this.props.user.username}</h3>

                        <p className='email'>{this.props.user.email}</p>
                    </div>
                    <form method='post' id='loginForm' onSubmit={() => this.handleFormSubmit()}>
                        <p className='help-block'></p>

                        <div className='form-group'>
                            <InputField type='password' placeholder='Old password' ref='oldPassword' />
                            <InputField type='password' placeholder='New password'
                                        dataType='password' ref='password' />
                            <InputField type='password' placeholder='Repeat new password'
                                        dataType='password' ref='repeat' />
                        </div>

                        <Button bsStyle='success' id='submit_reset' onClick={() => this.changePassword()} block>
                            Save new password
                        </Button>
                    </form>
                    <hr />
                    <div className='footer'>
                        <a className='btn btn-default btn-block'
                           onClick={() => this.props.switchModal('showProfileModal')}>
                            Go back
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}