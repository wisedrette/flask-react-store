import React from 'react';
import {  Col } from 'react-bootstrap'
import { DeliveryInput, DeliverySelect, ProfileInput, locationCheck } from './../../shared_components'

var DeliveryProfileForm = React.createClass({

    getValue() {
        return {
            address: this.refs.address.getValue(),
            state: this.refs.state.getValue(),
            zip: this.refs.zip.getValue(),
            phone: this.refs.phone.getValue()
        }
    },

    handleSave(event){
        var name = $(event.target).closest('.form-group').attr('class').replace('form-group ', '');
        switch (name) {
            case 'address':
                this.handleAddress(name);
                break;
            default:
                if (this.refs[name].validate())
                    this.props.saveChange(name, this.refs[name].getValue())
        }

    },

    handleChange(name){
        if (name == 'zip' && !this.refs[name].validate())
            return;
        this.props.saveChange(name, this.refs[name].getValue());
        if (name == 'state' && this.refs[name].getValue!=0)
            this.handleAddress(name);
    },

    addressMessageWarning(message){
        var classSelection = '.form-group.address';
        $(classSelection).find('.btn').addClass('hidden');
        $(classSelection).find('.error > i.fa').show().text(' ' + message)
            .delay(1500).fadeOut('slow', function () {
                $(classSelection).find('.error > i.fa').hide().text('');
                $(classSelection).find('.btn').removeClass('hidden');
            });
    },


    handleAddress(field){
        locationCheck(this.refs.state.getValue(), this.refs.address.getValue(), this.refs.zip.getValue(), function (status, result) {
            if (status) {
                if (result.message!= '') {
                    this.addressMessageWarning(result.message);
                    Object.keys(result.result).forEach(key => this.refs[key].setValue(result.result[key]));
                } else
                    Object.keys(result.result).forEach(key => this.props.saveChange(key, result.result[key]))
            } else {
                if (field == 'state') {
                    this.props.saveChange(field, this.refs[field].getValue());
                    this.refs.address.setValue('');
                }
                this.refs.address.toggleFieldError(false);
                this.addressMessageWarning(result.message);
            }
        }.bind(this));
    },

    render() {
        return (
            <form method='post' id='detailsForm' className='delivery'>
                <ProfileInput label='phone' action={true} onSave={this.handleSave}>
                    <DeliveryInput value={this.props.user.phone} name='phone' ref='phone' length={13}/>
                </ProfileInput>

                <ProfileInput label='state' action={false}>
                    <Col xs={8}>
                        <DeliverySelect value={this.props.user.state} ref='state' name='state' placeholder={0}
                                        onChange={this.handleChange}/>
                    </Col>
                    <Col xs={4}>
                        <DeliveryInput value={this.props.user.zip} ref='zip' name='zip' placeholder='zip'
                                       onChange={this.handleChange} length={5}/>
                    </Col>
                </ProfileInput>

                <ProfileInput label='address' action={true} onSave={this.handleSave} ref='addressWrapper'>
                    <DeliveryInput value={this.props.user.address} name='address' ref='address'/>
                </ProfileInput>

            </form>
        );
    }
});

export default DeliveryProfileForm;