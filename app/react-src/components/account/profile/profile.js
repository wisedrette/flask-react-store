import React from 'react';
import {  Image, Col } from 'react-bootstrap'
import { ProfileInput } from '../../shared_components'
import DeliveryProfileForm from './deliveryProfileForm.js'
import { ProfileModal, EmailChangeModal, PasswordChangeModal } from './modals/'

var Profile = React.createClass({

    getInitialState() {
        return {showProfileModal: false, showEmailChangeModal: false, showPasswordChangeModal: false};
    },

    switchModals(modal){
        var newState = {};
        Object.keys(this.state).forEach(key => newState[key] = key == modal);
        this.setState(newState);
    },

    modalsSection(){
        var user = {username: this.props.user.username, email: this.props.user.email, avatar: this.props.user.avatar}
        return (
            <section>
                <ProfileModal showModal={this.state.showProfileModal} user={user} switchModal={this.switchModals}
                              updateData={this.props.shouldUpdate} imageCentering={this.handleImageLoad}
                              close={() => this.setState({showProfileModal: false})}/>

                <EmailChangeModal showModal={this.state.showEmailChangeModal} user={user}
                                  switchModal={this.switchModals}
                                  updateData={this.props.shouldUpdate}
                                  close={() => this.setState({showEmailChangeModal: false})}/>

                <PasswordChangeModal showModal={this.state.showPasswordChangeModal} user={user}
                                     switchModal={this.switchModals}
                                     close={() => this.setState({showPasswordChangeModal: false})}/>
            </section>
        )
    },

    accountSection(){
        var password = [];
        for (var i = 0; i < 12; i++) {
            password.push(<i className='fa fa-circle' key={'p'+i}/>);
        }
        return (
            <section>

                <ProfileInput label='email' action={true} onSave={() => this.setState({showProfileModal: true})}
                              type='value'>
                    <strong>{this.props.user.email}</strong>
                </ProfileInput>

                <ProfileInput label='password' action={true} onSave={() => this.setState({showProfileModal: true})}
                              type='value'>
                    <strong className='password'>{password}</strong>
                </ProfileInput>

            </section>
        )
    },

    identitySection(){
        return (
            <section>
                <ProfileInput label='avatar' action={true} onSave={() => this.setState({showProfileModal: true})}
                              type='value'>
                    <div className='avatar-holder'>
                        <img src={this.props.user.avatar} className='avatar-img' alt='avatar' onLoad={this.handleImageLoad}/>
                    </div>
                </ProfileInput>

                <ProfileInput label='username' action={false} type='value'>
                    <strong>{this.props.user.username}</strong>
                </ProfileInput>
            </section>
        )
    },

    handleImageLoad(event){
        var width = event.target.naturalWidth,
            height = event.target.naturalHeight;
        if (width/height <= 0.9) {
            $(event.target).addClass('vertical');
            $(event.target).removeClass('horizontal');
        } else {
            if (width/height > 1.25)
                $(event.target).addClass('horizontal');
            $(event.target).removeClass('vertical');
        }
    },


    handleChange(field, value){
        this.props.saveChange(field, value);
    },

    render() {
        return (
            <Col xs={12} md={8} mdOffset={2} className='settings'>
                <h4>Account Login</h4>
                {this.accountSection()}

                <h4>My identity</h4>
                {this.identitySection()}

                <h4>Delivery details</h4>
                <DeliveryProfileForm saveChange={this.props.saveChange} user={this.props.user}/>

                {this.modalsSection()}
            </ Col>
        );
    }
});

export default Profile;