export { default as Profile } from './profile/profile.js';
export { default as Account } from './account.js'
export { default as Discounts } from './discounts.js'
export { default as OrdersHistory } from './ordersHistory.js'
