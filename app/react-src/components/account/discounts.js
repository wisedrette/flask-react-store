import React from 'react';
import { Button } from 'react-bootstrap';
import OrderListItem from '../shared_components/orderWrapper.js'
import InputField from '../shared_components/inputField.js'

export default class Discounts extends React.Component {
    constructor() {
        super();
        this.state = {discount: null};
    }

    componentDidMount() {
        $.ajax({
            method: 'GET',
            url: '/api/account',
            contentType: 'application/json',
            success: function (data) {
                this.setState({discount: data.user.discount});
            }.bind(this)
        });
    }

    submitDiscount() {
        $.ajax({
            method: 'POST',
            url: '/api/account/submit_discount',
            contentType: 'application/json',
            success: (function (data) {
                if (data.state) {
                    this.setState({discount: data.discount});
                } else
                    this.refs.code.setValidationError('Sorry! It seems that your discount code is incorrect')
            }.bind(this)),
            data: JSON.stringify({code: this.refs.code.getValue()})
        });
    }

    render() {
        if (this.state.discount == null || this.state.discount == 0) {
            return (
                <div className='discounts'>
                    <header className='upper'>You don't have any discounts</header>
                    <InputField type='text' placeholder='Enter your discount code' ref='code'/>
                    <Button bsStyle='success' onClick={() => this.submitDiscount()}>Submit</Button>
                </div>
            );
        } else
            return (
                <div className='discounts info'>
                    <h3 className='upper'>Congradulations! You have a {this.state.discount}% discount which can be applied to your purchase</h3>
                    <Button bsStyle='default' onClick={() => this.setState({discount: null})}>Redeem another discount code</Button>
                    <br/>
                    <small>Please note, that your discounts <strong>WON'T summarize!</strong> Redeeming another discount will cancel your current one!</small>
                </div>
            )
    }
}