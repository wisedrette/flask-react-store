import React from 'react';
import { Panel, Col, Image } from 'react-bootstrap';
import OrderListItem from '../shared_components/orderWrapper.js'

var OrdersHistory = React.createClass({
    getInitialState(){
        return ({orders: []})
    },

    componentDidMount(){
        $.ajax({
            method: 'GET',
            url: '/api/account/orders',
            contentType: 'application/json',
            success: function (data) {
                this.setState(data);
            }.bind(this)
        });
    },

    getOrdersHistory(){
        var orders = this.state.orders.map(order => <OrderListItem order={order} key={order.uid} isAdmin={false}/>);
        return (
            <Col xs={12} md={8} mdOffset={2}>
                {orders}
            </Col>
        )
    },

    render() {
        if (this.state.orders.length == 0)
            return (
                <div className='orders empty'>
                    <h3>You don't have any orders</h3>

                    <p>... yet :)</p>
                </div>
            );
        else
            return (
                <div className='orders'>
                    {this.getOrdersHistory()}
                </div>
            )
    }
});

export default OrdersHistory;