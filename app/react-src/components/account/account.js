import React from 'react';
import { Link } from 'react-router'
import { NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

var Profile = React.createClass({

    getInitialState() {
        return {
            user: {
                address: null,
                email: null,
                id: null,
                phone: null,
                avatar: null,
                state: null,
                username: null,
                zip: null
            }
        }
    },

    getUserData() {
        $.ajax({
            method: 'GET',
            url: '/api/account',
            contentType: 'application/json',
            success: function (data) {
                this.setState(data);
            }.bind(this)
        });
    },

    componentWillMount(){
        this.getUserData();
    },

    saveChange(field, value, displayMessage){
        var groupSelection = '.form-group.' + field;
        if (field == 'zip')
            groupSelection = '.form-group.state';
        var classSelection = groupSelection + '> .action';
        var data = {};
        data[field] = value;
        $.ajax({
            method: 'POST',
            url: '/api/account',
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function () {
                if (displayMessage || displayMessage == undefined) {
                    $(classSelection).find('.fa').hide().stop(true, true);
                    $(classSelection).find('.processing > .fa').show();
                }
            },
            success: function (data) {
                if (displayMessage || displayMessage == undefined)
                    $(classSelection).find('.processing > .fa').hide().closest('.action').find('.result > i.fa').show().text(data.message).delay(1500).fadeOut('slow');
                this.getUserData();
            }.bind(this),
            error: function () {
                $(classSelection).find('.processing > .fa').fadeOut('fast', function () {
                    $(groupSelection).find('.btn').addClass('hidden');
                    $(classSelection).find('.error > i.fa').show().text(' Something went wrong:(').delay(1500).fadeOut('slow', function () {
                        $(groupSelection).find('.btn').removeClass('hidden');
                    });
                });
                this.getUserData();
            }.bind(this)
        });
    },

    render() {
        if (isAuthenticated)
            return (
                <div className='content account'>
                    <div className='breadcrumbs'>
                        <Link to={'/account/orders'} activeClassName='active'>Orders history</Link>
                        <i className='fa fa-circle'/>
                        <Link to={'/account/settings'} activeClassName='active'>Profile settings</Link>
                        <i className='fa fa-circle'/>
                        <Link to={'/account/discounts'} activeClassName='active'>Discounts</Link>
                        <hr/>
                    </div>
                    {this.props.children && React.cloneElement(this.props.children, {
                        user: this.state.user, saveChange: this.saveChange, shouldUpdate: this.getUserData
                    })}
                </div>
            );
        else
            return (
                <div className='content center-block text-center alert alert-danger'>
                    <strong>Error!</strong> You haven't been authenticated. Please, login first.
                </div>
            )
    }
});

export default Profile;