import React from 'react'
import { Col } from 'react-bootstrap';
import OrderListItem from '../shared_components/orderWrapper.js'
import { connect } from 'react-redux'
//import io from 'socket.io-client'
import { addNewOrder, confirmOrder } from '../../redux/actions/actions.js'

//
//var url = document.domain + ':' + location.port,
//    socket = io.connect(url);


var AdminContent = React.createClass({

    getOrdersData() {
        $.ajax({
            method: 'GET',
            url: '/api/admin/orders',
            contentType: 'application/json',
            success: function (data) {
                const { dispatch } = this.props;
                data.orders.forEach(order => dispatch(addNewOrder(order)));
            }.bind(this)
        });
    },

    componentDidMount(){
        this.getOrdersData();
        //socket.on('post_order', function (data) {
        //    const { dispatch } = this.props;
        //    dispatch(addNewOrder(data.order));
        //}.bind(this));
    },

    confirmOrder(id){
        this.props.dispatch(confirmOrder(id));
        //socket.emit('confirm_order', id);
    },

    render() {
        const { orderList } = this.props;

        if (orderList.orders.length > 0) {
            var orders = orderList.orders.map(
                    order => <OrderListItem order={order} key={order.uid}
                                            isAdmin={true} confirm={this.confirmOrder}/>
            );

            return (
                <Col xs={12} md={8} mdOffset={2}>
                    {orders}
                </Col>
            )
        }

        else
            return (
                <div className='empty'>
                    <h3>There are no new orders</h3>

                    <p>... yet :)</p>
                </div>
            )
    }
});


function select(state) {
    return {
        orderList: state.orderList
    }
}

export default connect(select)(AdminContent)