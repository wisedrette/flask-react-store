import React from 'react'
import { render } from 'react-dom'
import AdminContent from './components/admin_components/adminContent.js'

import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { combineReducers } from 'redux';
import { orderList } from './redux/reducers';

const reducer = combineReducers({ orderList });
const store = createStore(reducer);


render(<Provider store={store}><AdminContent /></Provider>, document.getElementById('mount-point'));