import React from 'react'
import {  Modal } from 'react-bootstrap'
import NavbarMenu from './components/navigation/navbar.js'
import Footer from './components/navigation/footer.js'
//import io from 'socket.io-client'

//var url = document.domain + ':' + location.port,
//    socket = io.connect(url);

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {showModal: false, message: ''}
    }

    componentDidMount() {
        //console.log(messages);
        if (messages.length > 0)
            this.setState({showModal: true, message: messages})
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.showModal && !prevState.showModal)
            setTimeout(() => this.setState({showModal: false, message: ''}), 2000)
    }

    render() {
        var hideCartDropdown = false;
        if (/checkout/.test(this.props.location.pathname)) {
            hideCartDropdown = true;
        }
        return (
            <div className='container'>
                <NavbarMenu hideCartDropdown={hideCartDropdown}/>
                <div className="content-wrapper">
                    {this.props.children}
                    <div style={{clear:'both'}}></div>
                </div>
                <Footer />
                <FlashMessagesModal showModal={this.state.showModal} message={this.state.message}
                                    close={() => this.setState({showModal: false, message: ''})}/>
            </div>
        )
    }
}

class FlashMessagesModal extends React.Component {

    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.close} className='modalForm'>
                <Modal.Header closeButton/>
                <Modal.Body>
                    <h3 className="text-center">{this.props.message}</h3>
                </Modal.Body>
            </Modal>
        );
    }
}

export default App;