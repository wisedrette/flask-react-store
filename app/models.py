from app import db, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
# from flask.ext.login import
from flask_security import UserMixin, RoleMixin
from sqlalchemy.event import listens_for
from flask_security import SQLAlchemyUserDatastore
from slugify import slugify
from string import ascii_uppercase, digits
from random import SystemRandom

def id_generator(size=6):
    return ''.join(SystemRandom().choice(ascii_uppercase + digits) for _ in range(size))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }

    def __str__(self):
        return self.name


class Subcategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category', foreign_keys=[category_id])

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'category': self.category_id
        }

    def __str__(self):
        return self.name


class Type(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relation('Category', foreign_keys=[category_id])
    subcategory_id = db.Column(db.Integer, db.ForeignKey('subcategory.id'))
    subcategory = db.relation('Subcategory', foreign_keys=[subcategory_id])

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'category': self.category_id,
            'subcategory': self.subcategory_id
        }

    def __str__(self):
        return self.name


types_beers = db.Table(
    'types_beers',
    db.Column('type_id', db.Integer(), db.ForeignKey('type.id')),
    db.Column('beer_id', db.Integer(), db.ForeignKey('beer.id'))
)


class Beer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True)
    price = db.Column(db.Float, index=True)
    description = db.Column(db.Text())
    added = db.Column(db.TIMESTAMP, primary_key=False, index=True)
    sold = db.Column(db.Integer, index=True)
    img_path = db.Column(db.String(256))
    types = db.relationship('Type', secondary=types_beers,
                            backref=db.backref('beers', lazy='dynamic'))
    available = db.Column(db.Boolean, index=True)
    rate = db.Column(db.Float)
    path = db.Column(db.String(120), index=True)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'title': self.name,
            'price': self.price,
            'description': self.description,
            'img_path': self.img_path,
            'types_id': [types.id for types in self.types],
            'available': self.available,
            'sold': self.sold,
            'rate': self.rate,
            'path': self.path
        }

    @property
    def serialize_short(self):
        return {
            'id': self.id,
            'title': self.name,
            'price': self.price,
            'img_path': self.img_path,
            'types_id': [types.id for types in self.types],
            'available': self.available,
            'sold': self.sold,
            'rate': self.rate,
            'path': self.path
        }

    def __str__(self):
        return self.name


@listens_for(Beer, 'after_insert')
def add_news_path(mapper, connection, target):
    beers_table = Beer.__table__
    if target.name is not None and target.path is None:
        root = target.types[0].category.name
        root = '/'+root+'/'
        connection.execute(
            beers_table.update().
                where(beers_table.c.id == target.id).
                values(path=root+slugify(target.name))
        )


class BeerUserRate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relation('User', foreign_keys=[user_id])
    beer_id = db.Column(db.Integer, db.ForeignKey('beer.id'))
    beer = db.relation('Beer', foreign_keys=[beer_id])
    rate = db.Column(db.Integer)


@listens_for(BeerUserRate, 'after_insert')
@listens_for(BeerUserRate, 'after_delete')
def recalculate_rate(mapper, connection, target):
    beers_table = Beer.__table__
    beer_id = target.beer_id
    rates = [i.rate for i in BeerUserRate.query.filter_by(beer_id=beer_id).all()]
    if len(rates) > 0:
        rate = sum(rates)/len(rates)
    else:
        rate = 0
    if int(round(rate+0.25)) != int(round(rate)) or int(round(rate-0.25)) != int(round(rate)):
        rate = int(rate)+0.5
    else:
        rate = int(round(rate))
    connection.execute(
        beers_table.update().
            where(beers_table.c.id == target.beer_id).
            values(rate=rate)
    )


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    text = db.Column(db.Text())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relation('User', foreign_keys=[user_id])
    beer_id = db.Column(db.Integer, db.ForeignKey('beer.id'))
    beer = db.relation('Beer', foreign_keys=[beer_id])
    added_at = db.Column(db.TIMESTAMP, primary_key=False)

    @property
    def serialize(self):
        user = User.query.filter_by(id=self.user_id).first()
        if user is not None:
            user = user.username
        return {
            'title': self.title,
            'text': self.text,
            'added_at': self.added_at.isoformat(),
            'user': user
        }


class BeersInOrder(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    order = db.relation('Order', foreign_keys=[order_id])
    beer_id = db.Column(db.Integer, db.ForeignKey('beer.id'))
    beer = db.relation('Beer', foreign_keys=[beer_id])
    count = db.Column(db.Integer)

    def __str__(self):
        return Beer.query.filter(Beer.id == self.beer_id).first().name + ' x' + str(self.count)

    @property
    def serialize(self):
        beer = Beer.query.filter(Beer.id == self.beer_id).first()
        return {
            'id': self.beer_id,
            'name': beer.name,
            'img_path': beer.img_path,
            'price': beer.price,
            'count': self.count
        }


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    uid = db.Column(db.String(12))
    added_at = db.Column(db.TIMESTAMP, primary_key=False)
    confirmed_at = db.Column(db.TIMESTAMP, primary_key=False)
    delivered = db.Column(db.Boolean())
    discount = db.Column(db.Integer)
    state = db.Column(db.String(2), db.ForeignKey('state.code'))
    state_info = db.relation('State', foreign_keys=[state])
    address = db.Column(db.Text())
    phone = db.Column(db.String(32))
    zip = db.Column(db.String(5))
    total = db.Column(db.Float)
    beers = db.relationship('BeersInOrder', backref='orders', lazy='dynamic')

    def __str__(self):
        return str(self.id)

    @property
    def serialize(self):
        confirmed_at = self.confirmed_at
        if confirmed_at is not None:
            confirmed_at = confirmed_at.strftime('%s')
        return {
            'uid': self.uid,
            'added_at': self.added_at.strftime('%s'),
            'confirmed_at': confirmed_at,
            'delivered': self.delivered,
            'address': self.address + ' ' + self.zip + ' ' + self.state,
            'phone': self.phone,
            'beers': [i.serialize for i in self.beers],
            'total': self.total
        }

    @property
    def serialize_admin(self):
        confirmed_at = self.confirmed_at
        if confirmed_at is not None:
            confirmed_at = confirmed_at.strftime('%s')

        if self.user_id is not None:
            user = User.query.filter(User.id == self.user_id).first()
            user_serialized = {'username': user.username, 'email': user.email}
        else:
            user_serialized = {'username': None, 'email': None}

        return {
            'id': self.id,
            'uid': self.uid,
            'added_at': self.added_at.strftime('%s'),
            'confirmed_at': confirmed_at,
            'delivered': self.delivered,
            'address': self.address + ' ' + self.zip + ' ' + self.state,
            'phone': self.phone,
            'beers': [i.serialize for i in self.beers],
            'total': self.total,
            'user': user_serialized
        }


@listens_for(Order, 'after_delete')
def del_beers_relation(mapper, connection, target):
    BeersInOrder.query.filter(BeersInOrder.order_id.is_(None)).delete()


class News(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    text = db.Column(db.Text())
    img_path = db.Column(db.String(64))
    path = db.Column(db.String(128), index=True)
    added = db.Column(db.TIMESTAMP, primary_key=False, index=True)

    @property
    def serialize_preview(self):
        text = self.text
        if len(text) > 200:
            text = text[:200]
        return {
            'title': self.title,
            'description': text,
            'added': self.added.isoformat(),
            'img_path': self.img_path,
            'path': self.path
        }

    @property
    def serialize_short(self):
        return {
            'title': self.title,
            'path': self.path,
            'added': self.added.isoformat(),
        }

    @property
    def serialize(self):
        return {
            'title': self.title,
            'description': self.text,
            'added': self.added.isoformat(),
            'img_path': self.img_path,
            'path': self.path
        }


@listens_for(News, 'after_insert')
def add_news_path(mapper, connection, target):
    news_table = News.__table__
    if target.title is not None and target.path is None:
        connection.execute(
            news_table.update().
                where(news_table.c.id == target.id).
                values(path='/news/'+slugify(target.title))
        )


class State(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(2))
    name = db.Column(db.String(32))
    users = db.relationship('User', backref='state_code', lazy='dynamic')

    @property
    def serialize(self):
        return {
            'code': self.code,
            'name': self.name
        }

    def __str__(self):
        return self.name


roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class UserCodes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relation('User', foreign_keys=[user_id])


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)

    def __str__(self):
        return self.name


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    address = db.Column(db.Text())
    state = db.Column(db.String(2), db.ForeignKey('state.code'))
    phone = db.Column(db.String(32))
    zip = db.Column(db.String(5))
    avatar = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    orders = db.relationship('Order', backref='user', lazy='dynamic')
    discount = db.Column(db.Integer)

    def __str__(self):
        return self.email

    @property
    def serialize(self):
        return {
            'id': self.id,
            'email': self.email,
            'username': self.username,
            'address': self.address,
            'state': self.state,
            'phone': self.phone,
            'zip': self.zip,
            'avatar': self.avatar,
            'discount': self.discount
        }


class Discounts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(20))
    discount = db.Column(db.Integer)


@listens_for(Discounts, 'after_insert')
def add_news_path(mapper, connection, target):
    table = Discounts.__table__
    if target.discount is not None:
        code = id_generator(16)
        code = '-'.join(code[i:i + 4] for i in range(0, len(code), 4))
        connection.execute(
            table.update().
                where(table.c.id == target.id).
                values(code=code)
        )

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
