#!/usr/bin/env python
from flask import Flask
from flask_restful import Api
from flask.ext.sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_mail import Mail

from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.compress import Compress
from flask_webpack import Webpack
from flask_socketio import SocketIO


app = Flask(__name__)

webpack = Webpack()
api = Api(app)
app.config.from_object('config')
socketio = SocketIO(app)


login_manager = LoginManager()
login_manager.init_app(app)
mail = Mail(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

Bootstrap(app)

compress = Compress()
compress.init_app(app)
webpack.init_app(app)

from app import views, models

if __name__ == "__main__":
    app.run(host='0.0.0.0')
